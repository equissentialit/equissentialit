CREATE TABLE equissential.sequence
(
    SEQ_NAME character varying(50) NOT NULL,
    SEQ_COUNT smallint NOT NULL,
    CONSTRAINT "SEQUENCE_pkey" PRIMARY KEY (SEQ_NAME)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.sequence
    OWNER to postgres;

CREATE TABLE equissential.country
(
    id smallint NOT NULL,
    country_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    alpha_2 character(2) COLLATE pg_catalog."default" NOT NULL,
    alpha_3 character(3) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "COUNTRY_pkey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.country
    OWNER to postgres;

INSERT INTO equissential.country VALUES (1,'Afghanistan','af','afg'),(2,'Aland Islands','ax','ala'),(3,'Albania','al','alb'),(4,'Algeria','dz','dza'),(5,'American Samoa','as','asm'),(6,'Andorra','ad','and'),(7,'Angola','ao','ago'),(8,'Anguilla','ai','aia'),(9,'Antarctica','aq',''),(10,'Antigua and Barbuda','ag','atg'),(11,'Argentina','ar','arg'),(12,'Armenia','am','arm'),(13,'Aruba','aw','abw'),(14,'Australia','au','aus'),(15,'Austria','at','aut'),(16,'Azerbaijan','az','aze'),(17,'Bahamas','bs','bhs'),(18,'Bahrain','bh','bhr'),(19,'Bangladesh','bd','bgd'),(20,'Barbados','bb','brb'),(21,'Belarus','by','blr'),(22,'Belgium','be','bel'),(23,'Belize','bz','blz'),(24,'Benin','bj','ben'),(25,'Bermuda','bm','bmu'),(26,'Bhutan','bt','btn'),(27,'Bolivia, Plurinational State of','bo','bol'),(28,'Bonaire, Sint Eustatius and Saba','bq','bes'),(29,'Bosnia and Herzegovina','ba','bih'),(30,'Botswana','bw','bwa'),(31,'Bouvet Island','bv',''),(32,'Brazil','br','bra'),(33,'British Indian Ocean Territory','io',''),(34,'Brunei Darussalam','bn','brn'),(35,'Bulgaria','bg','bgr'),(36,'Burkina Faso','bf','bfa'),(37,'Burundi','bi','bdi'),(38,'Cambodia','kh','khm'),(39,'Cameroon','cm','cmr'),(40,'Canada','ca','can'),(41,'Cape Verde','cv','cpv'),(42,'Cayman Islands','ky','cym'),(43,'Central African Republic','cf','caf'),(44,'Chad','td','tcd'),(45,'Chile','cl','chl'),(46,'China','cn','chn'),(47,'Christmas Island','cx',''),(48,'Cocos (Keeling) Islands','cc',''),(49,'Colombia','co','col'),(50,'Comoros','km','com'),(51,'Congo','cg','cog'),(52,'Congo, The Democratic Republic of the','cd','cod'),(53,'Cook Islands','ck','cok'),(54,'Costa Rica','cr','cri'),(55,'Cote d''Ivoire','ci','civ'),(56,'Croatia','hr','hrv'),(57,'Cuba','cu','cub'),(58,'Curacao','cw','cuw'),(59,'Cyprus','cy','cyp'),(60,'Czech Republic','cz','cze'),(61,'Denmark','dk','dnk'),(62,'Djibouti','dj','dji'),(63,'Dominica','dm','dma'),(64,'Dominican Republic','do','dom'),(65,'Ecuador','ec','ecu'),(66,'Egypt','eg','egy'),(67,'El Salvador','sv','slv'),(68,'Equatorial Guinea','gq','gnq'),(69,'Eritrea','er','eri'),(70,'Estonia','ee','est'),(71,'Ethiopia','et','eth'),(72,'Falkland Islands (Malvinas)','fk','flk'),(73,'Faroe Islands','fo','fro'),(74,'Fiji','fj','fji'),(75,'Finland','fi','fin'),(76,'France','fr','fra'),(77,'French Guiana','gf','guf'),(78,'French Polynesia','pf','pyf'),(79,'French Southern Territories','tf',''),(80,'Gabon','ga','gab'),(81,'Gambia','gm','gmb'),(82,'Georgia','ge','geo'),(83,'Germany','de','deu'),(84,'Ghana','gh','gha'),(85,'Gibraltar','gi','gib'),(86,'Greece','gr','grc'),(87,'Greenland','gl','grl'),(88,'Grenada','gd','grd'),(89,'Guadeloupe','gp','glp'),(90,'Guam','gu','gum'),(91,'Guatemala','gt','gtm'),(92,'Guernsey','gg','ggy'),(93,'Guinea','gn','gin'),(94,'Guinea-Bissau','gw','gnb'),(95,'Guyana','gy','guy'),(96,'Haiti','ht','hti'),(97,'Heard Island and McDonald Islands','hm',''),(98,'Holy See (Vatican City State)','va','vat'),(99,'Honduras','hn','hnd'),(100,'Hong Kong','hk','hkg'),(101,'Hungary','hu','hun'),(102,'Iceland','is','isl'),(103,'India','in','ind'),(104,'Indonesia','id','idn'),(105,'Iran, Islamic Republic of','ir','irn'),(106,'Iraq','iq','irq'),(107,'Ireland','ie','irl'),(108,'Isle of Man','im','imn'),(109,'Israel','il','isr'),(110,'Italy','it','ita'),(111,'Jamaica','jm','jam'),(112,'Japan','jp','jpn'),(113,'Jersey','je','jey'),(114,'Jordan','jo','jor'),(115,'Kazakhstan','kz','kaz'),(116,'Kenya','ke','ken'),(117,'Kiribati','ki','kir'),(118,'Korea, Democratic People''s Republic of','kp','prk'),(119,'Korea, Republic of','kr','kor'),(120,'Kuwait','kw','kwt'),(121,'Kyrgyzstan','kg','kgz'),(122,'Lao People''s Democratic Republic','la','lao'),(123,'Latvia','lv','lva'),(124,'Lebanon','lb','lbn'),(125,'Lesotho','ls','lso'),(126,'Liberia','lr','lbr'),(127,'Libyan Arab Jamahiriya','ly','lby'),(128,'Liechtenstein','li','lie'),(129,'Lithuania','lt','ltu'),(130,'Luxembourg','lu','lux'),(131,'Macao','mo','mac'),(132,'Macedonia, The former Yugoslav Republic of','mk','mkd'),(133,'Madagascar','mg','mdg'),(134,'Malawi','mw','mwi'),(135,'Malaysia','my','mys'),(136,'Maldives','mv','mdv'),(137,'Mali','ml','mli'),(138,'Malta','mt','mlt'),(139,'Marshall Islands','mh','mhl'),(140,'Martinique','mq','mtq'),(141,'Mauritania','mr','mrt'),(142,'Mauritius','mu','mus'),(143,'Mayotte','yt','myt'),(144,'Mexico','mx','mex'),(145,'Micronesia, Federated States of','fm','fsm'),(146,'Moldova, Republic of','md','mda'),(147,'Monaco','mc','mco'),(148,'Mongolia','mn','mng'),(149,'Montenegro','me','mne'),(150,'Montserrat','ms','msr'),(151,'Morocco','ma','mar'),(152,'Mozambique','mz','moz'),(153,'Myanmar','mm','mmr'),(154,'Namibia','na','nam'),(155,'Nauru','nr','nru'),(156,'Nepal','np','npl'),(157,'Netherlands','nl','nld'),(158,'New Caledonia','nc','ncl'),(159,'New Zealand','nz','nzl'),(160,'Nicaragua','ni','nic'),(161,'Niger','ne','ner'),(162,'Nigeria','ng','nga'),(163,'Niue','nu','niu'),(164,'Norfolk Island','nf','nfk'),(165,'Northern Mariana Islands','mp','mnp'),(166,'Norway','no','nor'),(167,'Oman','om','omn'),(168,'Pakistan','pk','pak'),(169,'Palau','pw','plw'),(170,'Palestinian Territory, Occupied','ps','pse'),(171,'Panama','pa','pan'),(172,'Papua New Guinea','pg','png'),(173,'Paraguay','py','pry'),(174,'Peru','pe','per'),(175,'Philippines','ph','phl'),(176,'Pitcairn','pn','pcn'),(177,'Poland','pl','pol'),(178,'Portugal','pt','prt'),(179,'Puerto Rico','pr','pri'),(180,'Qatar','qa','qat'),(181,'Reunion','re','reu'),(182,'Romania','ro','rou'),(183,'Russian Federation','ru','rus'),(184,'Rwanda','rw','rwa'),(185,'Saint Barthelemy','bl','blm'),(186,'Saint Helena, Ascension and Tristan Da Cunha','sh','shn'),(187,'Saint Kitts and Nevis','kn','kna'),(188,'Saint Lucia','lc','lca'),(189,'Saint Martin (French Part)','mf','maf'),(190,'Saint Pierre and Miquelon','pm','spm'),(191,'Saint Vincent and The Grenadines','vc','vct'),(192,'Samoa','ws','wsm'),(193,'San Marino','sm','smr'),(194,'Sao Tome and Principe','st','stp'),(195,'Saudi Arabia','sa','sau'),(196,'Senegal','sn','sen'),(197,'Serbia','rs','srb'),(198,'Seychelles','sc','syc'),(199,'Sierra Leone','sl','sle'),(200,'Singapore','sg','sgp'),(201,'Sint Maarten (Dutch Part)','sx','sxm'),(202,'Slovakia','sk','svk'),(203,'Slovenia','si','svn'),(204,'Solomon Islands','sb','slb'),(205,'Somalia','so','som'),(206,'South Africa','za','zaf'),(207,'South Georgia and The South Sandwich Islands','gs',''),(208,'South Sudan','ss','ssd'),(209,'Spain','es','esp'),(210,'Sri Lanka','lk','lka'),(211,'Sudan','sd','sdn'),(212,'Suriname','sr','sur'),(213,'Svalbard and Jan Mayen','sj','sjm'),(214,'Swaziland','sz','swz'),(215,'Sweden','se','swe'),(216,'Switzerland','ch','che'),(217,'Syrian Arab Republic','sy','syr'),(218,'Taiwan, Province of China','tw',''),(219,'Tajikistan','tj','tjk'),(220,'Tanzania, United Republic of','tz','tza'),(221,'Thailand','th','tha'),(222,'Timor-Leste','tl','tls'),(223,'Togo','tg','tgo'),(224,'Tokelau','tk','tkl'),(225,'Tonga','to','ton'),(226,'Trinidad and Tobago','tt','tto'),(227,'Tunisia','tn','tun'),(228,'Turkey','tr','tur'),(229,'Turkmenistan','tm','tkm'),(230,'Turks and Caicos Islands','tc','tca'),(231,'Tuvalu','tv','tuv'),(232,'Uganda','ug','uga'),(233,'Ukraine','ua','ukr'),(234,'United Arab Emirates','ae','are'),(235,'United Kingdom','gb','gbr'),(236,'United States','us','usa'),(237,'United States Minor Outlying Islands','um',''),(238,'Uruguay','uy','ury'),(239,'Uzbekistan','uz','uzb'),(240,'Vanuatu','vu','vut'),(241,'Venezuela, Bolivarian Republic of','ve','ven'),(242,'Viet Nam','vn','vnm'),(243,'Virgin Islands, British','vg','vgb'),(244,'Virgin Islands, U.S.','vi','vir'),(245,'Wallis and Futuna','wf','wlf'),(246,'Western Sahara','eh','esh'),(247,'Yemen','ye','yem'),(248,'Zambia','zm','zmb'),(249,'Zimbabwe','zw','zwe');

CREATE TABLE equissential.person
(
    id bigserial NOT NULL,
    first_name character varying(50) NOT NULL,
    surname character varying(50) NOT NULL,
    email character varying(75),
    password character(168),
    mobile_number character varying(20),
    phone_number character varying(20),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.person
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('PERSON_SEQ',0);

CREATE TABLE equissential.organisation
(
    id bigserial NOT NULL,
    organisation_name character varying(100),
    logo_path character varying(100),
    web_site character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ORGANISATION_SEQ',0);

CREATE TABLE equissential.address
(
    id bigserial NOT NULL,
    name character varying(20),
    address character varying(256),
    city character varying(75),
    country_id smallint,
    country_subdivision character varying(75),
    postal_code character varying(15),
    person_id bigint,
    PRIMARY KEY (id),
    CONSTRAINT country_id FOREIGN KEY (country_id)
        REFERENCES equissential.country (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT person_id FOREIGN KEY (person_id)
        REFERENCES equissential.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.address
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ADDRESS_SEQ',0);
 
CREATE TABLE equissential.horse_colour
(
    id bigserial NOT NULL,
    colour_name character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.horse_colour
    OWNER to postgres;

INSERT INTO equissential.horse_colour VALUES 
    (1, 'Bay'), (2, 'Bay Roan'), (3, 'Bay Roan with  White Blanket'), (4, 'Bay with White Blanket'), (5, 'Black'), (6, 'Blank Dun'),
    (7, 'Black with White Blanket'), (8, 'Blue Roan'), (9, 'Charcoal'), (10, 'Charcoal with White Blanket'), (11,'Chestnut'),
    (12, 'Chestnut with White Blanket'), (13,'Cream'), (14, 'Cremello'), (15, 'Dapple Grey'), (16,'Dark Bay'),
    (17,'Dark Bay with White Blanket'),(18, 'Dun'), (19, 'Buckskin'), (20, 'Fleabitten Grey'), (21, 'Grey'), (22, 'Leopard Spotted'),
    (23, 'Liver Chestnut'), (24, 'Palomino'), (25, 'Piebald'), (26, 'Skewbald'), (27, 'Overo'), (28, 'Sabino'), (29, 'Tobiano'),
    (30, 'Tovero'), (31, 'Rabicano'), (32, 'Red Roan'), (33, 'Smokey Black'), (34, 'White'), (35,'Rose Grey'), (36, 'Tri-Coloured'),
    (37, 'Piebald Tobiano'), (38, 'Skewbald Tobiano'), (39, 'Tri-Coloured Tobiano');

CREATE TABLE equissential.horse_breed
(
    id bigserial NOT NULL,
    breed_name character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.horse_breed
    OWNER to postgres;

INSERT INTO equissential.horse_breed VALUES
    (1, 'American Quarter Horse'), (2, 'American Quarter Horse Cross'), (3, 'American Saddle Horse'), (4, 'American Saddle Horse Cross'), 
    (5, 'American Paint Horse'), (6, 'American Warmblood'), (7, 'Andalusian'),(8, 'Andalusian Cross'), (9, 'Arabian'), (10, 'Arabian Cross'), 
    (11, 'Anglo-Arab'), (12, 'Appaloosa'), (13, 'Appaloosa Cross'), (14, 'Belgian Warmblood'), (15, 'Boerperd'), (16, 'Boerperd Cross'),
    (17, 'Clydesdale'), (18, 'Clydesdale Cross'), (19, 'Czech Warmblood'), (20, 'Danish Warmblood'), (21, 'Dutch Warmblood'), 
    (22, 'Oldenburg'), (23, 'Friesian'), (24, 'Friesian Cross'), (25, 'Gypsy Vanner'), (26, 'Hanoverian'), (27, 'Holsteiner'),
    (28, 'Hungarian Warmblood'), (29, 'Irish Draught'), (30, 'Irish Sport Horse'), (31, 'Lippizaner'), (32, 'Miniture horse'),
    (33, 'Namibian Warmblood'), (34, 'Nooitgedachter'), (35, 'Percheron'), (36, 'Percheron Cross'), (37, 'Pintabian'), (38, 'Selle Français'),
    (39, 'Shire'), (40, 'Shire Cross'), (41, 'Swedish Warmblood'), (42, 'Swiss Warmblood'), (43, 'Thoroughbred'), (44, 'Trakehner'),
    (45, 'Warmblood'), (46, 'Warmblood Cross'), (47, 'Welsh Section A'), (48, 'Welsh Section B'), (49, 'Welsh Section C'),
    (50, 'Welsh Cob - Section D'), (51, 'Shetland Pony'), (52, 'Basotho Pony'), (53, 'Basotho Pony Cross'), (54, 'Welsh Cross'),
    (55, 'Nooitgedachter Cross'), (56, 'Connemara'), (57, 'Connemara Cross'), (58, 'South African Warmblood'), (59, 'Thoroughred Cross');

CREATE TABLE equissential.horse
(
    id bigserial NOT NULL,
    horse_name character varying(50) NOT NULL,
    passport_number character varying(75),
    date_of_birth timestamp,
    sire character varying(75),
    dam character varying(75),
    sire_of_dam character varying(75),
    colour_id bigint,
    breed_id bigint,
    country_of_origin_id bigint,
    gender character varying(10),
    owner_id bigint,
    horse_classification character varying(10),
    height character varying(10),
    PRIMARY KEY (id),
    CONSTRAINT owner_id FOREIGN KEY (owner_id)
        REFERENCES equissential.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT colour_id FOREIGN KEY (colour_id)
        REFERENCES equissential.horse_colour (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT breed_id FOREIGN KEY (breed_id)
        REFERENCES equissential.horse_breed (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT country_of_origin_id FOREIGN KEY (country_of_origin_id)
        REFERENCES equissential.country (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.horse
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('HORSE_SEQ',0);


CREATE TABLE equissential.organisation_type
(
    id bigserial NOT NULL,
    name character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation_type
    OWNER to postgres;

INSERT INTO equissential.organisation_type VALUES
    (1,'Riding School'),
    (2,'Stud'),
    (3,'Private Yard'),
    (4, 'Association'),
    (5,'Club'),
    (6,'Showholding Body');

CREATE TABLE equissential.organisation_member
(
    id bigserial NOT NULL,
    membership_number character varying(50),
    organisation_id bigint NOT NULL,
    person_id bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT person_id FOREIGN KEY (person_id)
        REFERENCES equissential.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT organisation_id FOREIGN KEY (organisation_id)
        REFERENCES equissential.organisation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation_member
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ORGANISATION_MEMBER_SEQ',0);

CREATE TABLE equissential.organisation_member_horse
(
    id bigserial NOT NULL,
    horse_membership_number character varying(50),
    horse_id bigint NOT NULL,
    organisation_member_id bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT organisation_member_id FOREIGN KEY (organisation_member_id)
        REFERENCES equissential.organisation_member (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT horse_id FOREIGN KEY (horse_id)
        REFERENCES equissential.horse (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation_member_horse
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ORGANISATION_MEMBER_HORSE_SEQ',0);

CREATE TABLE equissential.organisation_member_role
(
    id bigserial NOT NULL,
    organisation_role character varying(50) NOT NULL,
    organisation_member_id bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT organisation_member_id FOREIGN KEY (organisation_member_id)
        REFERENCES equissential.organisation_member (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation_member_role
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ORGANISATION_MEMBER_ROLE_SEQ',0);

CREATE TABLE equissential.email_spool
(
    id bigserial NOT NULL,
    to_address character varying(256),
    subject character varying(256),
    cc_address character varying(256),
    bcc_address character varying(256),
    from_address character varying(256),
    reply_to character varying(256),
    date_submitted timestamp,
    status character varying(10),
    content_type character(4),
    body text,
    attachment bytea,
    attachment_name character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.email_spool
    OWNER to postgres;

CREATE TABLE equissential.audit
(
    id bigserial NOT NULL,
    task_time character varying(50),
    person character varying(100),
    service character varying(50),
    task character varying(50),
    execution_time integer,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.audit
    OWNER to postgres;

CREATE TABLE equissential.person_role
(
    email character varying(75) NOT NULL,
    p_role character varying(25) NOT NULL,
    PRIMARY KEY (email, p_role)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.person_role
    OWNER to postgres;

CREATE TABLE equissential.account_create_request
(
    id character(168) NOT NULL,
    date_submitted timestamp,
    email character varying(75) NOT NULL,
    status character varying(10),
    date_processed timestamp,
    first_name character varying(50),
    surname character varying(50),
    password character(168),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.account_create_request
    OWNER to postgres;

CREATE TABLE equissential.organisation_join_request
(
    organisation_id bigint NOT NULL,
    person_id bigint NOT NULL,
    status character varying(10),
    date_submitted timestamp,
    date_processed timestamp,
    decline_reason character varying(200),
    PRIMARY KEY (organisation_id, person_id),
    CONSTRAINT organisation_id FOREIGN KEY (organisation_id)
        REFERENCES equissential.organisation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT person_id FOREIGN KEY (person_id)
        REFERENCES equissential.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.organisation_join_request
    OWNER to postgres;

CREATE TABLE equissential.password_change_request
(
    id character(168) NOT NULL,
    date_submitted timestamp,
    email character varying(75),
    status character varying(10),
    date_processed timestamp
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.password_change_request
    OWNER to postgres;

CREATE TABLE equissential.show_venue
(
    id bigserial NOT NULL,
    venue_name character varying(100),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show_venue
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('SHOW_VENUE_SEQ',0);

CREATE TABLE equissential.arena
(
    id bigserial NOT NULL,
    arena_name character varying(100) NOT NULL,
    show_venue_id bigserial NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT show_venue_id FOREIGN KEY (show_venue_id)
        REFERENCES equissential.show_venue (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.arena
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('ARENA_SEQ',0);

CREATE TABLE equissential.show
(
    id bigserial NOT NULL,
    show_name character varying(100),
    start_date date,
    end_date date,
    show_venue_id bigserial NOT NULL,
    organisation_id bigserial NOT NULL,
    competitor_organisation_member boolean,
    PRIMARY KEY (id),
    CONSTRAINT show_venue_id FOREIGN KEY (show_venue_id)
        REFERENCES equissential.show_venue (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT organisation_id FOREIGN KEY (organisation_id)
        REFERENCES equissential.Organisation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('SHOW_SEQ',0);

CREATE TABLE equissential.show_program
(
    id bigserial NOT NULL,
    show_date date,
    start_time time,
    arena_id bigserial,
    show_id bigserial NOT NULL,
    arena_has_preference boolean,
    schedule_completed boolean,
    PRIMARY KEY (id),
    CONSTRAINT show_id FOREIGN KEY (show_id)
        REFERENCES equissential.show (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT arena_id FOREIGN KEY (arena_id)
        REFERENCES equissential.arena (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show_program
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('SHOW_PROGRAM_SEQ',0);

CREATE TABLE equissential.break_time
(
    id bigserial NOT NULL,
    break_time time NOT NULL,
    break_duration_minutes int NOT NULL,
    show_program_id bigserial NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT show_program_id FOREIGN KEY (show_program_id)
        REFERENCES equissential.show_program (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.break_time
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('BREAK_TIME_SEQ',0);

CREATE TABLE equissential.class_type
(
    id bigserial NOT NULL,
    class_type_name character varying(100),
    sub_type character varying(100),
    course_walk_time_minutes bigint,
    time_per_competitor_minutes bigint,
    time_for_prizegiving bigint,
    display_rider_start_time boolean,
    class_type_has_preference boolean,
    PRIMARY KEY(id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.class_type
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('CLASS_TYPE_SEQ',0);


CREATE TABLE equissential.show_class
(
    id bigserial NOT NULL,
    class_code character varying(50) NOT NULL,
    class_name character varying(100),
    test_link character varying(100),
    test_details character varying(100),
    test_name character varying(100),
    class_level smallint,
    show_program_id bigserial NOT NULL,
    approx_start_time time,
    arena_order smallint,
    class_type_id bigserial,
    follow_on_class bigserial,
    is_follow_on boolean,
    PRIMARY KEY(id),
    CONSTRAINT show_program_id FOREIGN KEY (show_program_id)
        REFERENCES equissential.show_program (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT follow_on_class FOREIGN KEY (follow_on_class)
        REFERENCES equissential.show_class (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT class_type_id FOREIGN KEY (class_type_id)
        REFERENCES equissential.class_type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show_class
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('SHOW_CLASS_SEQ',0);

CREATE TABLE equissential.show_entry
(
    id bigserial NOT NULL,
    rider_number character varying(20),
    person_id bigserial NOT NULL,
    horse_id bigserial NOT NULL,
    school_name character varying(100),
    approx_start_time time,
    running_order smallint,
    entry_swopped boolean,
    PRIMARY KEY (id),
    CONSTRAINT person_id FOREIGN KEY (person_id)
        REFERENCES equissential.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT horse_id FOREIGN KEY (horse_id)
        REFERENCES equissential.horse (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show_entry
    OWNER to postgres;

INSERT INTO equissential.sequence VALUES ('SHOW_ENTRY_SEQ',0);

CREATE TABLE equissential.show_class_show_entry
(
    ShowClass_id bigserial NOT NULL,
    showEntries_id bigserial NOT NULL,
    PRIMARY KEY (ShowClass_id,showEntries_id),
    CONSTRAINT ShowClass_id FOREIGN KEY (ShowClass_id)
        REFERENCES equissential.show_class (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT showEntries_id FOREIGN KEY (showEntries_id)
        REFERENCES equissential.show_entry (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE equissential.show_class_show_entry
    OWNER to postgres;


-- Test data
INSERT INTO equissential.organisation VALUES (1, 'River Ridge Stud', '','');
INSERT INTO equissential.organisation VALUES (2, 'Equissential Academy', '','');
INSERT INTO equissential.organisation VALUES (3, 'SANESA KZN', '','www.sanesa.co.za');

UPDATE equissential.sequence SET SEQ_COUNT = 3 WHERE SEQ_NAME = 'ORGANISATION_SEQ';

INSERT INTO equissential.person VALUES (1, 'Liezel', 'Bedggood', 'liezelbedggood@gmail.com', '120000:d71ecce18e9699bbed8fea394daa8923:62c7d899463aba980de3ea7274ed242c3bffc88c6f5c55643666fc42fd8b7c9a2bd13b3b462979dee4145e9e2067ce24f79bf077687fc40d2207f90550bf9d47', '074 447 2133', '');
INSERT INTO equissential.person VALUES (2, 'Sarah', 'Bedggood', 'sarahbedgie@gmail.com', '120000:8ee40dd6b3fe7597965db90ef4a944d8:cab9d12c6d128c624f53af39378391d743dc7b53fd50cffc34a618cef60babf79cf00866d1c3605bbe81a39f0df75a13516938636f9a07bb36b6e6be10e34c25', '074 447 2133', '');

UPDATE equissential.sequence SET SEQ_COUNT = 2 WHERE SEQ_NAME = 'PERSON_SEQ';

INSERT INTO equissential.address VALUES (1, 'Physical Address', 'River Ridge Farm//nPortion 5 of The Bend', 'Nottingham Road', 206, 'Kwazulu-Natal', '3280', 1);
INSERT INTO equissential.address VALUES (2, 'Postal Address', 'P.O. Box 278', 'Nottingham Road', 206, 'Kwazulu-Natal', '3280', 1);
INSERT INTO equissential.address VALUES (3, 'Physical Address', 'River Ridge Farm//nPortion 5 of The Bend', 'Nottingham Road', 206, 'Kwazulu-Natal', '3280', 2);
INSERT INTO equissential.address VALUES (4, 'Postal Address', 'P.O. Box 278', 'Nottingham Road', 206, 'Kwazulu-Natal', '3280', 2);

UPDATE equissential.sequence SET SEQ_COUNT = 4 WHERE SEQ_NAME = 'ADDRESS_SEQ';

INSERT INTO equissential.horse VALUES (1, 'Saratoga Pablo Picasso', '12345', to_date('2010-12-05', 'YYYY-MM-DD'), 'Saratoga''s Waldemar', 'Saratoga Sprungli', 'Drabant', 37, 58, 206, 'Stallion', 1, 'Horse', '15.2hh');
INSERT INTO equissential.horse VALUES (2, 'River Ridge Lindor', '67890', to_date('2005-10-05', 'YYYY-MM-DD'), 'Unknown', 'Unknown', 'unknown', 1, 59, 206, 'Mare', 1, 'Horse', '15.3hh');
INSERT INTO equissential.horse VALUES (3, 'River Ridge Greystoke', '2007/2321', to_date('2012-10-05', 'YYYY-MM-DD'), 'Unknown', 'Unknown', 'unknown', 21, 59, 206, 'Gelding', 2, 'Pony', '14hh');

UPDATE equissential.sequence SET SEQ_COUNT = 3 WHERE SEQ_NAME = 'HORSE_SEQ';

INSERT INTO equissential.organisation_member VALUES (1, '111111', 1, 1);
INSERT INTO equissential.organisation_member VALUES (2, '222222', 1, 2);

UPDATE equissential.sequence SET SEQ_COUNT = 2 WHERE SEQ_NAME = 'ORGANISATION_MEMBER_SEQ';

INSERT INTO equissential.organisation_member_horse VALUES (1, 374839, 1, 1);
INSERT INTO equissential.organisation_member_horse VALUES (2, 464565, 2, 1);
INSERT INTO equissential.organisation_member_horse VALUES (3, 374839, 3, 2);

UPDATE equissential.sequence SET SEQ_COUNT = 3 WHERE SEQ_NAME = 'ORGANISATION_MEMBER_HORSE_SEQ';

INSERT INTO equissential.person_role VALUES ('liezelbedggood@gmail.com', 'EquissentialUser');
INSERT INTO equissential.person_role VALUES ('sarahbedgie@gmail.com', 'EquissentialUser');

INSERT INTO equissential.class_type VALUES (1, 'Showjumping','Competition',10,3,5,false,false);
INSERT INTO equissential.class_type VALUES (2, 'Showjumping','Precision & Speed',10,2,5,false,false);
INSERT INTO equissential.class_type VALUES (3, 'Showjumping','Ideal Time',10,2,5,false,false);
INSERT INTO equissential.class_type VALUES (4, 'Dressage','',0,3,0,false,true);
INSERT INTO equissential.class_type VALUES (5, 'Equitation','',15,5,5,false,false);
INSERT INTO equissential.class_type VALUES (6, 'Performance Riding','',0,5,0,false,false);
INSERT INTO equissential.class_type VALUES (7, 'Working Riding','',0,5,0,false,false);
INSERT INTO equissential.class_type VALUES (8, 'Working Hunter','',0,5,0,false,false);

UPDATE equissential.sequence SET SEQ_COUNT = 8 WHERE SEQ_NAME = 'CLASS_TYPE_SEQ';

INSERT INTO equissential.show_venue VALUES (1, 'Canterbury Equestrian Centre');

UPDATE equissential.sequence SET SEQ_COUNT = 1 WHERE SEQ_NAME = 'SHOW_VENUE_SEQ';

INSERT INTO equissential.arena VALUES(1, 'Sand Arena',1);
INSERT INTO equissential.arena VALUES(2, 'Sand Arena 1',1);
INSERT INTO equissential.arena VALUES(3, 'Sand Arena 2',1);
INSERT INTO equissential.arena VALUES(4, 'Main Grass Arena',1);
INSERT INTO equissential.arena VALUES(5, 'Showing Arena 1',1);
INSERT INTO equissential.arena VALUES(6, 'Showing Arena 2',1);
INSERT INTO equissential.arena VALUES(7, 'Dressage Arena 1',1);
INSERT INTO equissential.arena VALUES(8, 'Dressage Arena 2',1);

UPDATE equissential.sequence SET SEQ_COUNT = 8 WHERE SEQ_NAME = 'ARENA_SEQ';

INSERT INTO equissential.show VALUES(1,'SANESA Qualifier 1',to_date('2018-02-03', 'YYYY-MM-DD'), to_date('2018-02-04', 'YYYY-MM-DD'), 1, 3, false);

UPDATE equissential.sequence SET SEQ_COUNT = 1 WHERE SEQ_NAME = 'SHOW_SEQ';


INSERT INTO equissential.show_program VALUES (2, to_date('2018-02-03', 'YYYY-MM-DD'), '08:15:00', 8, 1, true); /* Dressage */
INSERT INTO equissential.show_program VALUES (1, to_date('2018-02-03', 'YYYY-MM-DD'), '08:00:00', 7, 1, true); /* Dressage */
INSERT INTO equissential.show_program VALUES (3, to_date('2018-02-03', 'YYYY-MM-DD'), '08:00:00', 4, 1, false); /* Equitation */
INSERT INTO equissential.show_program VALUES (4, to_date('2018-02-03', 'YYYY-MM-DD'), '08:00:00', 5, 1, false); /* Working Hunter */
INSERT INTO equissential.show_program VALUES (5, to_date('2018-02-04', 'YYYY-MM-DD'), '08:00:00', 5, 1, false); /* Performance Riding, Working Riding */
INSERT INTO equissential.show_program VALUES (6, to_date('2018-02-04', 'YYYY-MM-DD'), '08:00:00', 4, 1, false); /* Showjumping */
INSERT INTO equissential.show_program VALUES (7, to_date('2018-02-04', 'YYYY-MM-DD'), '08:00:00', 1, 1, false); /* Showjumping */

UPDATE equissential.sequence SET SEQ_COUNT = 7 WHERE SEQ_NAME = 'SHOW_PROGRAM_SEQ';

INSERT INTO equissential.break_time VALUES (1, '10:30:00', 15, 1);
INSERT INTO equissential.break_time VALUES (2, '13:00:00', 15, 1);
INSERT INTO equissential.break_time VALUES (3, '10:30:00', 15, 2);
INSERT INTO equissential.break_time VALUES (4, '13:00:00', 15, 2);
INSERT INTO equissential.break_time VALUES (5, '10:30:00', 15, 5);
INSERT INTO equissential.break_time VALUES (6, '13:00:00', 15, 5);
INSERT INTO equissential.break_time VALUES (7, '15:00:00', 15, 5);

UPDATE equissential.sequence SET SEQ_COUNT = 8 WHERE SEQ_NAME = 'BREAK_TIME_SEQ';

INSERT INTO equissential.show_class VALUES (1, 'P0SJ01a (9 & younger) Off Lead', 'Level 0 Showjumping Ideal Time - (9 & younger) Off Lead', '', '', '', 0, 6, '00:00:00', 1, 3);
INSERT INTO equissential.show_class VALUES (2, 'P0SJ02a (9 & younger) Off Lead', 'Level 0 Showjumping Competition - (9 & younger) Off Lead', '', '', '', 0, 6, '00:00:00', 6, 1); 
INSERT INTO equissential.show_class VALUES (3, 'P0SJ01b (10 up) Off Lead', 'Level 0 Showjumping Ideal Time - (10 up) Off Lead', '', '', '', 0, 6, '00:00:00', 2, 3); 
INSERT INTO equissential.show_class VALUES (4, 'P0SJ02b (10 up) Off Lead', 'Level 0 Showjumping Competition - (10 up) Off Lead', '', '', '', 0, 6, '00:00:00', 7, 1);  
INSERT INTO equissential.show_class VALUES (5, 'P1SJ03', 'Level 1 Showjumping Ideal Time', '', '', '', 1, 6, '00:00:00', 3, 3); 
INSERT INTO equissential.show_class VALUES (6, 'P1SJ04', 'Level 1 Showjumping Competition', '', '', '', 1, 6, '00:00:00', 8, 1); 
INSERT INTO equissential.show_class VALUES (7, 'P2SJ05', 'Level 2 Showjumping Ideal Time', '', '', '', 2, 6, '00:00:00', 4, 3); 
INSERT INTO equissential.show_class VALUES (8, 'P2SJ06', 'Level 2 Showjumping Competition', '', '', '', 2, 6, '00:00:00', 9, 1); 
INSERT INTO equissential.show_class VALUES (9, 'P3SJ07', 'Level 3 Showjumping Competition', '', '', '', 3, 6, '00:00:00', 5, 1); 
INSERT INTO equissential.show_class VALUES (10, 'P3SJ08', 'Level 3 Showjumping Precision & Speed', '', '', '', 3, 6, '00:00:00', 10, 2); 
INSERT INTO equissential.show_class VALUES (11, 'P4SJ09', 'Level 4 Showjumping Competition', '', '', '', 4, 7, '00:00:00', 5, 1); 
INSERT INTO equissential.show_class VALUES (12, 'P4SJ10', 'Level 4 Showjumping Precision & Speed', '', '', '', 4, 7, '00:00:00', 1, 2); 
INSERT INTO equissential.show_class VALUES (13, 'P5SJ11', 'Level 5 Showjumping Competition', '', '', '', 5, 7, '00:00:00', 6, 1); 
INSERT INTO equissential.show_class VALUES (14, 'P5SJ12', 'Level 5 Showjumping Precision & Speed', '', '', '', 5, 7, '00:00:00', 2, 2); 
INSERT INTO equissential.show_class VALUES (15, 'P6SJ13', 'Level 6 Showjumping Competition', '', '', '', 6, 7, '00:00:00', 7, 1); 
INSERT INTO equissential.show_class VALUES (16, 'P6SJ14', 'Level 6 Showjumping Precision & Speed', '', '', '', 6, 7, '00:00:00', 3, 2); 
INSERT INTO equissential.show_class VALUES (17, 'P7SJ15', 'Level 7 Showjumping Competition', '', '', '', 7, 7, '00:00:00', 8, 1); 
INSERT INTO equissential.show_class VALUES (18, 'P7SJ16', 'Level 7 Showjumping Precision & Speed', '', '', '', 7, 7, '00:00:00', 4, 2); 
INSERT INTO equissential.show_class VALUES (19, 'P0EQ01a (9 & younger) Lead Rein', 'Level 0 Equitation - (9 & younger) Lead Rein', '', '', '', 0, 3, '00:00:00', 1, 5); 
INSERT INTO equissential.show_class VALUES (20, 'P0EQ01b (10 up) Lead Rein', 'Level 0 Equittion - (9 & younger) Off Lead', '', '', '', 0, 3, '00:00:00', 2, 5); 
INSERT INTO equissential.show_class VALUES (21, 'P0EQ02a (9 & younger) Off Lead', 'Level 0 Showjumping Competition - (9 & younger) Off Lead', '', '', '', 0, 3, '00:00:00', 3, 5); 
INSERT INTO equissential.show_class VALUES (22, 'P0EQ02b (10 up) Off Lead', 'Level 0 Showjumping Competition - (9 & younger) Off Lead', '', '', '', 0, 3, '00:00:00', 4, 5); 
INSERT INTO equissential.show_class VALUES (23, 'P1EQ03', 'Level 1 Equitation', '', '', '', 1, 3, '00:00:00', 5, 5); 
INSERT INTO equissential.show_class VALUES (24, 'P2EQ04', 'Level 2 Equitation', '', '', '', 2, 3, '00:00:00', 6, 5); 
INSERT INTO equissential.show_class VALUES (25, 'P4EQ05', 'Level 4 Equitation', '', '', '', 4, 3, '00:00:00', 7, 5); 
INSERT INTO equissential.show_class VALUES (26, 'P6EQ06', 'Level 6 Equitation', '', '', '', 6, 3, '00:00:00', 8, 5); 
INSERT INTO equissential.show_class VALUES (27, 'P1WH01', 'Level 1 Working Hunter', '', '', '', 1, 4, '00:00:00', 1, 8); 
INSERT INTO equissential.show_class VALUES (28, 'P3WH02', 'Level 3 Working Hunter', '', '', '', 3, 4, '00:00:00', 2, 8); 
INSERT INTO equissential.show_class VALUES (29, 'P5WH03', 'Level 5 Working Hunter', '', '', '', 5, 4, '00:00:00', 3, 8); 
INSERT INTO equissential.show_class VALUES (30, 'P7WH04', 'Level 7 Working Hunter', '', '', '', 7, 4, '00:00:00', 4, 8); 
INSERT INTO equissential.show_class VALUES (31, 'P0WR01a (9 & younger) Lead Rein', 'Level 0 Working Riding (9 & Younger) Lead Rein', '', '', '', 0, 5, '00:00:00', 1, 7); 
INSERT INTO equissential.show_class VALUES (32, 'P0WR01b (10 up) Lead Rein', 'Level 0 Working Riding - (9 & younger) Lead Rein', '', '', '', 0, 5, '00:00:00', 2, 7); 
INSERT INTO equissential.show_class VALUES (33, 'P0WR02a (9 & younger) Off Lead', 'Level 0 Working Riding - (9 & younger) Off Lead', '', '', '', 0, 5, '00:00:00', 3, 7); 
INSERT INTO equissential.show_class VALUES (34, 'P0WR02b (10 up) Off Lead', 'Level 0 Working Riding - (9 & younger) Off Lead', '', '', '', 0, 5, '00:00:00', 4, 7); 
INSERT INTO equissential.show_class VALUES (35, 'P1WR03', 'Level 1 Working Riding', '', '', '', 1, 5, '00:00:00', 5, 7); 
INSERT INTO equissential.show_class VALUES (36, 'P3WR04', 'Level 3 Working Riding', '', '', '', 3, 5, '00:00:00', 6, 7); 
INSERT INTO equissential.show_class VALUES (37, 'P5WR05', 'Level 5 Working Riding', '', '', '', 5, 5, '00:00:00', 7, 7); 
INSERT INTO equissential.show_class VALUES (38, 'P0PR01a (9 & younger) Lead Rein', 'Level 0 Performance Riding - (9 & younger) Lead Rein', '', '', '', 0, 5, '00:00:00', 8, 6); 
INSERT INTO equissential.show_class VALUES (39, 'P0PR01b (10 up) Lead Rein', 'Level 0 Performance Riding - (9 & younger) Lead Rein', '', '', '', 0, 5, '00:00:00', 9, 6); 
INSERT INTO equissential.show_class VALUES (40, 'P0PR02a (9 & younger) Off Lead', 'Level 0 Performance Riding - (9 & younger) Off Lead', '', '', '', 0, 5, '00:00:00', 10, 6); 
INSERT INTO equissential.show_class VALUES (41, 'P0PR02b (10 up) - Off Lead', 'Level 0 Performance Riding - (9 & younger) Off Lead', '', '', '', 0, 5, '00:00:00', 11, 6); 
INSERT INTO equissential.show_class VALUES (42, 'P1PR03', 'Level 1 Performance Riding', '', '', '', 1, 5, '00:00:00', 12, 6); 
INSERT INTO equissential.show_class VALUES (43, 'P3PR04', 'Level 3 Performance Riding', '', '', '', 3, 5, '00:00:00', 13, 6); 
INSERT INTO equissential.show_class VALUES (44, 'P5PR05', 'Level 5 Performance Riding', '', '', '', 5, 5, '00:00:00', 14, 6); 
INSERT INTO equissential.show_class VALUES (45, 'P0DR01a - (9 & younger) Lead Rein', 'Level 0 Dressage a - (9 & younger) Off Lead', '', '', '', 0, 1, '00:00:00', 1, 4); 
INSERT INTO equissential.show_class VALUES (46, 'P0DR01b - (9 & younger) Lead Rein', 'Level 0 Dressage b - (9 & younger) Off Lead', '', '', '', 0, 2, '00:00:00', 1, 4); 
INSERT INTO equissential.show_class VALUES (47, 'P0DR02a (10 up) Lead Rein', 'Level 0 Level 0 Dressage a - (9 & younger) Off Lead', '', '', '', 0, 1, '00:00:00', 2, 4); 
INSERT INTO equissential.show_class VALUES (48, 'P0DR02b (10 up) Lead Rein', 'Level 0 Dressage b - (9 & younger) Off Lead', '', '', '', 0, 2, '00:00:00', 2, 4); 
INSERT INTO equissential.show_class VALUES (49, 'P0DR03a - (9 & younger) Off Lead', 'Level 0 Dressage a - (9 & younger) Off Lead', '', '', '', 0, 1, '00:00:00', 3, 4); 
INSERT INTO equissential.show_class VALUES (50, 'P0DR03b - (9 & younger) Off Lead', 'Level 0 Dressage b - (9 & younger) Off Lead', '', '', '', 0, 2, '00:00:00', 3, 4); 
INSERT INTO equissential.show_class VALUES (51, 'P0DR04a - (10 up) Off Lead', 'Level 0 Dressage a - (9 & younger) Off Lead', '', '', '', 0, 1, '00:00:00', 4, 4); 
INSERT INTO equissential.show_class VALUES (52, 'P0DR04b - (10 up) Off Lead', 'Level 0 Dressage b - (9 & younger) Off Lead', '', '', '', 0, 2, '00:00:00', 4, 4); 
INSERT INTO equissential.show_class VALUES (53, 'P1DR05a', 'Level 1 Dressage a', '', '', '', 1, 1, '00:00:00', 5, 4); 
INSERT INTO equissential.show_class VALUES (54, 'P1DR05b', 'Level 1 Dressage b', '', '', '', 1, 2, '00:00:00', 5, 4); 
INSERT INTO equissential.show_class VALUES (55, 'P2DR06a', 'Level 2 Dressage a', '', '', '', 2, 1, '00:00:00', 6, 4); 
INSERT INTO equissential.show_class VALUES (56, 'P2DR06b', 'Level 2 Dressage b', '', '', '', 2, 2, '00:00:00', 6, 4); 
INSERT INTO equissential.show_class VALUES (57, 'P3DR07a', 'Level 3 Dressage a', '', '', '', 3, 1, '00:00:00', 7, 4); 
INSERT INTO equissential.show_class VALUES (58, 'P3DR07b', 'Level 3 Dressage b', '', '', '', 3, 2, '00:00:00', 7, 4); 
INSERT INTO equissential.show_class VALUES (59, 'P4DR08a', 'Level 4 Dressage a', '', '', '', 4, 1, '00:00:00', 8, 4); 
INSERT INTO equissential.show_class VALUES (60, 'P4DR08b', 'Level 4 Dressage b', '', '', '', 4, 2, '00:00:00', 8, 4); 
INSERT INTO equissential.show_class VALUES (61, 'P5DR08a', 'Level 5 Dressage a', '', '', '', 5, 1, '00:00:00', 9, 4); 
INSERT INTO equissential.show_class VALUES (62, 'P5DR08b', 'Level 5 Dressage b', '', '', '', 5, 2, '00:00:00', 9, 4); 
INSERT INTO equissential.show_class VALUES (63, 'P6DR10a', 'Level 6 Dressage a', '', '', '', 6, 1, '00:00:00', 10, 4); 
INSERT INTO equissential.show_class VALUES (64, 'P6DR10b', 'Level 6 Dressage b', '', '', '', 6, 2, '00:00:00', 10, 4); 
INSERT INTO equissential.show_class VALUES (65, 'P7DR11a', 'Level 7 Dressage a', '', '', '', 7, 1, '00:00:00', 11, 4); 
INSERT INTO equissential.show_class VALUES (66, 'P7DR11b', 'Level 7 Dressage b', '', '', '', 7, 2, '00:00:00', 11, 4); 

UPDATE equissential.sequence SET SEQ_COUNT = 66 WHERE SEQ_NAME = 'SHOW_CLASS_SEQ';

