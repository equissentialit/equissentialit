package za.co.equissential.auth.controller;

import za.co.equissential.auth.RegisterStatus;
import za.co.equissential.auth.web.RegistrationRequest;
import za.co.equissential.auth.entity.AccountCreateRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import za.co.equissential.notification.NotificationController;

import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.auth.ResetPasswordStatus;
import za.co.equissential.person.entity.Person;
import za.co.equissential.exceptions.EntityExistsException;
import za.co.equissential.exceptions.SecurityException;
import za.co.equissential.person.entity.PersonRole;
import za.co.equissential.person.entity.PersonRolePK;
import za.co.equissential.util.PasswordUtil;
import za.co.equissential.util.TokenUtil;

/**
 * Session Bean implementation class AuthenticationController
 */
@Stateless
@LocalBean
@Interceptors(AuditInterceptor.class)
@PermitAll
public class RegisterController {

    @PersistenceContext
    private EntityManager entityManager;
    
    @EJB
    private NotificationController notificationController;    

    public RegisterController() {
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Create a new account create request.
     *
     * @param registrationRequest the account to create
     * @throws SecurityException if the password cannot be hashed
     * @throws za.co.equissential.exceptions.EntityExistsException if the email address is already in use
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createRegisterRequest(@Valid RegistrationRequest registrationRequest) throws SecurityException, EntityExistsException {
        doesEmailExist(registrationRequest);
        String token = TokenUtil.generateToken();
        saveAccountCreateRequest(token, registrationRequest);
        notificationController.submitRegisterRequest(token, registrationRequest);
    }

    /**
     * Check if the email for this registration requests is already in use.
     *
     * @param registrationTO The registration request.
     */
    private void doesEmailExist(RegistrationRequest registrationRequest) throws EntityExistsException {
        Query query = entityManager.createNamedQuery("Person.findByEmail");
        query.setParameter("email", registrationRequest.getEmail());
        try {
            query.getSingleResult();
            throw new EntityExistsException("Person with this email address exists.");
        } catch (NoResultException noResultException) {
            // do nothing - the email address does not exist
        }
    }

    /**
     * Save the account create request to the database.
     * 
     * @param token the token to hash and store
     * @param registrationTO the person creating the account
     * @throws SecurityException if a hash cannot be generated
     */
    private void saveAccountCreateRequest(String token, RegistrationRequest registrationRequest) throws SecurityException {
        AccountCreateRequest accountCreateRequest = new AccountCreateRequest();
        accountCreateRequest.setId(TokenUtil.generateStrongTokenHash(token));
        accountCreateRequest.setName(registrationRequest.getName());
        accountCreateRequest.setSurname(registrationRequest.getSurname());
        accountCreateRequest.setEmail(registrationRequest.getEmail());
        accountCreateRequest.setDateSubmitted(LocalDateTime.now());
        String hashPassword = PasswordUtil.generateStrongPasswordHash(registrationRequest.getPassword());
        accountCreateRequest.setPassword(hashPassword);
        accountCreateRequest.setStatus(RegisterStatus.PENDING.value());
        entityManager.persist(accountCreateRequest);
    }
    
    /**
     * Verify the account create request.
     *
     * @param email The email of the person requesting the request.
     * @param token The token provided for the reset.
     * @return True if it is a valid request, otherwise False.
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean verifyAccountCreateRequest(String token, String email) {
        Query query = entityManager.createNamedQuery("AccountCreateRequest.findPendingRequestByEmail");
        query.setParameter("email", email);
        try {
            AccountCreateRequest accountCreateRequest = (AccountCreateRequest) query.getSingleResult();
            boolean verified = TokenUtil.validateToken(token, accountCreateRequest.getId());
            if (verified) {
                createAccount(accountCreateRequest);
                accountCreateRequest.setStatus(ResetPasswordStatus.PROCESSED.value());
                accountCreateRequest.setDateProcessed(LocalDateTime.now());
                entityManager.merge(accountCreateRequest);
            }
            return verified;
        } catch (NoResultException noResultException) {
            return false;
        }
    }
    
    /**
     * Create the person (after verification).
     * @param accountCreateRequest the person to create.
     */
    private void createAccount(AccountCreateRequest accountCreateRequest) {
        Person person = new Person();
        person.setEmail(accountCreateRequest.getEmail());
        person.setFirstName(accountCreateRequest.getName());
        person.setSurname(accountCreateRequest.getSurname());
        person.setPassword(accountCreateRequest.getPassword());
        entityManager.persist(person);
        
        PersonRolePK personRolePK = new PersonRolePK(accountCreateRequest.getEmail(), "EquissentialUser");
        PersonRole personRole = new PersonRole(personRolePK);
        entityManager.persist(personRole);
    }
    
}
