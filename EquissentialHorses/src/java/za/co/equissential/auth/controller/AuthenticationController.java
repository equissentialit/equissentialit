package za.co.equissential.auth.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;


import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.exceptions.SecurityException;
import za.co.equissential.util.PasswordUtil;

import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.auth.web.Credentials;
import za.co.equissential.person.entity.Person;

/**
 * Session Bean implementation class AuthenticationController
 */
@Stateless
@LocalBean
@Interceptors(AuditInterceptor.class)
@PermitAll
public class AuthenticationController {

    @Context
    private HttpServletRequest httpRequest;

    @PersistenceContext
    private EntityManager entityManager;

    public AuthenticationController() {
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Try to login with the credentials supplied.
     *
     * @param loginTO the login credentials
     * @throws SecurityException if some error occurs while calling HttpRequest.login
     */
    public void login(@Valid Credentials loginTO) throws SecurityException {
        try {
            Person person = getPerson(loginTO.getEmail());
            String storedPassword = person.getPassword();
            String hashedPassword = PasswordUtil.hashPassword(loginTO.getPassword(), storedPassword);
            httpRequest.getSession(true);
            httpRequest.login(loginTO.getEmail(), hashedPassword);
            if (!httpRequest.isUserInRole("EquissentialUser")) {
                throw new SecurityException("Invalid role.");
            }
        } catch (ServletException | EntityNotFoundException exception) {
            throw new SecurityException("Login failed", exception);
        }
    }

    /**
     * Get the person for the specified email address.
     *
     * @param email the email address to search for
     * @return the person associated to the email address
     * @throws za.co.cf.emma.rest.exceptions.EntityNotFoundException if the
     * email address is not found
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Person getPerson(String email) throws EntityNotFoundException {
        Query query = entityManager.createNamedQuery("Person.findByEmail");
        query.setParameter("email", email);
        try {
            return (Person) query.getSingleResult();
        }
        catch (NoResultException noResultException) {
            throw new EntityNotFoundException("Entity " + Person.class.getSimpleName() + " with id " + email + " not found.");
        }
    }
    
    /**
     * Log the user out.
     * 
     * @return true if the logout call is successful, otherwise false. 
     */
    public boolean logout() {
        boolean logoutSuccess = true;
        try {
            httpRequest.logout();
            httpRequest.getSession().invalidate();
        } catch (ServletException ex) {
            logoutSuccess = false;
            Logger.getLogger(AuthenticationController.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return logoutSuccess;
    }
}