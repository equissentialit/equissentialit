package za.co.equissential.auth.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.auth.ResetPasswordStatus;
import za.co.equissential.auth.entity.PasswordChangeRequest;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.notification.NotificationController;
import za.co.equissential.person.entity.Person;
import za.co.equissential.util.PasswordUtil;
import za.co.equissential.util.TokenUtil;


/**
 * Session Bean implementation class PersonController
 */
@Stateless
@LocalBean
@PermitAll
@Interceptors(AuditInterceptor.class)
public class ResetPasswordController {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private NotificationController notificationController;
    
    @EJB
    private AuthenticationController authenticationController;

    /**
     * Default constructor.
     */
    public ResetPasswordController() {
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Process the password change request. All pending requests for this person
     * must be canceled and the new request added.
     *
     * @param email the email of the person that requested the password reset
     * @throws za.co.equissential.exceptions.EntityNotFoundException if the person is not found
     * @throws za.co.equissential.exceptions.SecurityException when a token cannot be generated
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void processPasswordChangeRequest(String email) throws EntityNotFoundException, SecurityException {
        Person person = authenticationController.getPerson(email);
        String token = TokenUtil.generateToken();
        cancelPendingForgotPasswordRequests(person);
        saveForgotPasswordRequest(token, person);
        notificationController.submitResetPasswordRequest(token, person);
    }

    /**
     * Verify the password reset action.
     *
     * @param email The email of the person requesting the request.
     * @param token The token provided for the reset.
     * @return True if it is a valid request, otherwise False.
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean verifyResetPasswordRequest(String token, String email) {
        Query query = entityManager.createNamedQuery("PasswordChangeRequest.findPendingRequestsByEmail");
        query.setParameter("email", email);
        try {
            PasswordChangeRequest passwordChangeRequest = (PasswordChangeRequest) query.getSingleResult();
            boolean verified = TokenUtil.validateToken(token, passwordChangeRequest.getId());
            if (verified) {
                passwordChangeRequest.setStatus(ResetPasswordStatus.VERIFIED.value());
                passwordChangeRequest.setDateProcessed(LocalDateTime.now());
                entityManager.merge(passwordChangeRequest);
            }
            return verified;
        } catch (NoResultException noResultException) {
            return false;
        }
    }

    /**
     * Cancel pending forgot password requests, when a new one is received.
     *
     * @param person the person to check for pending password requests.
     */
    private void cancelPendingForgotPasswordRequests(Person person) {
        Query query = entityManager.createNamedQuery("PasswordChangeRequest.findPendingOrVerifiedRequestsByEmail");
        query.setParameter("email", person.getEmail());
        List<PasswordChangeRequest> passwordChangeRequests = query.getResultList();
        for (PasswordChangeRequest passwordChangeRequest : passwordChangeRequests) {
            passwordChangeRequest.setStatus(ResetPasswordStatus.CANCELLED.value());
            passwordChangeRequest.setDateProcessed(LocalDateTime.now());
            entityManager.merge(passwordChangeRequest);
        }
    }

    /**
     * Save the password change request to the database.
     *
     * @param token the token to hash and store
     * @param person the person that requested the password reset
     * @throws SecurityException if a hash cannot be generated
     */
    private void saveForgotPasswordRequest(String token, Person person) throws SecurityException {
        PasswordChangeRequest changeRequest = new PasswordChangeRequest();
        changeRequest.setId(TokenUtil.generateStrongTokenHash(token));
        changeRequest.setEmail(person.getEmail());
        changeRequest.setDateSubmitted(LocalDateTime.now());
        changeRequest.setStatus(ResetPasswordStatus.PENDING.value());
        entityManager.persist(changeRequest);
    }

    /**
     * Reset the password.
     *
     * @param email The email of the person whose password must be reset.
     * @param password The new password.
     * @param token The token for the reset password request.
     * @return True if the update was successful, otherwise False.
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean resetPassword(String email, String password, String token) {
        try {
            String hashedPassword = PasswordUtil.generateStrongPasswordHash(password);
            Query query = entityManager.createNamedQuery("PasswordChangeRequest.findVerifiedRequestsByEmail");
            query.setParameter("email", email);
            try {
                PasswordChangeRequest passwordChangeRequest = (PasswordChangeRequest) query.getSingleResult();
                boolean verified = TokenUtil.validateToken(token, passwordChangeRequest.getId());
                if (verified) {
                    passwordChangeRequest.setStatus(ResetPasswordStatus.PROCESSED.value());
                    passwordChangeRequest.setDateProcessed(LocalDateTime.now());
                    entityManager.merge(passwordChangeRequest);
                }
                
                Person person = authenticationController.getPerson(email);
                person.setPassword(hashedPassword);
                entityManager.merge(person);
                
                return verified;
            } catch (NoResultException | EntityNotFoundException ex) {
                Logger.getLogger(ResetPasswordController.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } catch (za.co.equissential.exceptions.SecurityException ex) {
            Logger.getLogger(ResetPasswordController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
