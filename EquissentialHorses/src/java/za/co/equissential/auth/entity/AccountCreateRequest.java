package za.co.equissential.auth.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The persistent class for the "ACCOUNT_CREATE_REQUEST" database table.
 */
@Entity
@Table(name = "account_create_request", schema = "equissential")
@NamedQueries({
    @NamedQuery(name = "AccountCreateRequest.findByEmail", 
            query = "SELECT a FROM AccountCreateRequest a WHERE a.email = :email"),
    @NamedQuery(name = "AccountCreateRequest.findPendingRequestByEmail", 
            query = "SELECT a FROM AccountCreateRequest a WHERE a.email = :email AND a.status = 'Pending'")    
    
})
public class AccountCreateRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "id")
    @Id
    private String id;

    @Column(name = "date_submitted")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateSubmitted;  

    @Basic(optional = false)
    @Column(name = "email", length = 75)
    private String email;
    
    @NotNull
    @Column(name = "first_name")
    private String name;

    @NotNull
    @Column(name = "surname")
    private String surname;

    @XmlTransient
    @Column(name = "password")
    private String password;    

    @Column(name = "status")
    private String status;
    
    @Column(name = "date_processed")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateProcessed;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(LocalDateTime dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public LocalDateTime getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(LocalDateTime dateProcessed) {
        this.dateProcessed = dateProcessed;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AccountCreateRequest)) {
            return false;
        }
        AccountCreateRequest other = (AccountCreateRequest) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "za.co.equissential.notification.PasswordChangeRequest[ id=" + id + " ]";
    }

}
