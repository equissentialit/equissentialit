package za.co.equissential.auth.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

/**
 * The persistent class for the "PASSWORD_CHANGE_REQUEST" database table.
 */
@Entity
@Table(name = "password_change_request", schema = "equissential")
@NamedQueries({
    @NamedQuery(name = "PasswordChangeRequest.findPendingOrVerifiedRequestsByEmail", 
            query = "SELECT p FROM PasswordChangeRequest p WHERE p.email = :email AND p.status IN ('Pending', 'Verified')"),
    @NamedQuery(name = "PasswordChangeRequest.findPendingRequestsByEmail", 
            query = "SELECT p FROM PasswordChangeRequest p WHERE p.email = :email AND p.status = 'Pending'"),
    @NamedQuery(name = "PasswordChangeRequest.findVerifiedRequestsByEmail", 
            query = "SELECT p FROM PasswordChangeRequest p WHERE p.email = :email AND p.status = 'Verified'")
})
public class PasswordChangeRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "id")
    @Id
    private String id;

    @Column(name = "date_submitted")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateSubmitted;

    @Basic(optional = false)
    @Column(name = "email", length = 75)
    private String email;

    @Column
    private String status;
    
    @Column(name = "date_processed")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateProcessed;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(LocalDateTime dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public LocalDateTime getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(LocalDateTime dateProcessed) {
        this.dateProcessed = dateProcessed;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PasswordChangeRequest)) {
            return false;
        }
        PasswordChangeRequest other = (PasswordChangeRequest) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "za.co.equissential.auth.entity.PasswordChangeRequest[ id=" + id + " ]";
    }

}
