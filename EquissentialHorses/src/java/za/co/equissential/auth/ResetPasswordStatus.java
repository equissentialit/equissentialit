package za.co.equissential.auth;

/**
 * Defines the states of a password reset.
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>Verified - the user used the password link, but not reset the password.</li>
 *      <li>Pending - user has requested a password reset, but has not processed it yet.</li>
 *      <li>Processed - the user has successfully reset their password.</li>
 *      <li>Expired - the user did not process the password request.</li>
 *      <li>Canceled - the user requested a new password reset, any existing requests are canceled.</li>
 *  </ul> 
 */
public enum ResetPasswordStatus {

    VERIFIED("Verified"),
    PENDING("Pending"),
    PROCESSED("Processed"),
    EXPIRED("Expired"),
    CANCELLED("Canceled");

    private final String resetPasswordStatus;

    ResetPasswordStatus(String resetPasswordStatus) {
        this.resetPasswordStatus = resetPasswordStatus;
    }

    public String value() {
        return resetPasswordStatus;
    }  
}
