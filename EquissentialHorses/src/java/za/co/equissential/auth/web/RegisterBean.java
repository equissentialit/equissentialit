package za.co.equissential.auth.web;

import za.co.equissential.util.BaseBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import za.co.equissential.auth.controller.RegisterController;
import za.co.equissential.exceptions.EntityExistsException;
import za.co.equissential.exceptions.SecurityException;

@Named
@RequestScoped
public class RegisterBean extends BaseBean {

    @EJB
    private RegisterController registerController;

    private String email;
    private String name;
    private String surname;
    private String password;
    /* The token provided for the password reset request. */
    private String tk;
    /* The email provided for the password reset request. */
    private String ml;
    private boolean validRequest;
    
    /**
     * Creates a new instance of RegisterManagedBean
     */
    public RegisterBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTk() {
        return tk;
    }

    public void setTk(String tk) {
        this.tk = tk;
    }

    public String getMl() {
        return ml;
    }

    public void setMl(String ml) {
        this.ml = ml;
    }

    public boolean isValidRequest() {
        return validRequest;
    }

    public void setValidRequest(boolean validRequest) {
        this.validRequest = validRequest;
    }

    public void validate() {
        validRequest = registerController.verifyAccountCreateRequest(tk, ml);
    }

    public String register() {
        try {
            RegistrationRequest registrationRequest = new RegistrationRequest();
            registrationRequest.setName(this.getName());
            registrationRequest.setSurname(this.getSurname());
            registrationRequest.setPassword(this.getPassword());
            registrationRequest.setEmail(this.getEmail());
            registerController.createRegisterRequest(registrationRequest);
            return "registerConfirm?faces-redirect=true";
        } catch (SecurityException ex) {
            Logger.getLogger(RegisterBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EntityExistsException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, getBundle().getString("registerFailure"), null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return null;
    }
}
