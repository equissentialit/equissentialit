package za.co.equissential.auth.web;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import za.co.equissential.auth.controller.ResetPasswordController;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.util.BaseBean;

/**
 * Manages the forgot password flow.
 */
@Named
@RequestScoped
public class ForgotPasswordBean extends BaseBean {

    @EJB
    private ResetPasswordController resetPasswordController;

    private String emailAddress;

    /**
     * Creates a new instance of ForgotPasswordBean
     */
    public ForgotPasswordBean() {
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Send the password change request.
     *
     * @return the forgot password success page
     */
    public String sendPasswordChangeRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!validEmail()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, getBundle().getString("changePassword"), getBundle().getString("invalidEmail"));
            context.addMessage(null, msg);
        } else {
            try {
                resetPasswordController.processPasswordChangeRequest(emailAddress);
                return "resetPasswordConfirm?faces-redirect=true";
            } catch (EntityNotFoundException ex) {
                Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.WARNING, "EMail that does not exist was supplied for \"Forget Password\"");
                return "resetPasswordConfirm?faces-redirect=true";
            } catch (SecurityException ex) {
                Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.SEVERE, null, ex);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, getBundle().getString("resetPasswordFailure"), null));
            }
        }
        return null;
    }

    /**
     * Validate the email address.
     *
     * @return true if the address is valid, otherwise false.
     */
    private boolean validEmail() {
        try {
            new InternetAddress(getEmailAddress()).validate();
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }
}
