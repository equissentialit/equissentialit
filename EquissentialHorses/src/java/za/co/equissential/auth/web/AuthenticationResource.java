package za.co.equissential.auth.web;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import za.co.equissential.auth.controller.AuthenticationController;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.person.entity.Person;


/**
 * Session Bean implementation class RegisterResource
 */
@Provider
@Path("auth")
public class AuthenticationResource {

    @EJB
    private AuthenticationController authenticationController;

    public AuthenticationResource() {
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(Credentials credentials) throws za.co.equissential.exceptions.SecurityException {
        authenticationController.login(credentials);
        return Response.ok().build();
    }
    
    @GET
    @Path("person/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPerson(@PathParam("email") String email) throws EntityNotFoundException {
        return authenticationController.getPerson(email);
    }
    
    @POST
    @Path("logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout() throws SecurityException {
        boolean logoutSuccess = authenticationController.logout();
        if (logoutSuccess) {
            return Response.ok().build();
        }
        else {
            return Response.serverError().build();
        }
    }
}