package za.co.equissential.auth.web;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import za.co.equissential.auth.controller.ResetPasswordController;

/**
 * The ResetPasswordBean deals with the reset password screens.
 */
@Named
@RequestScoped
public class ResetPasswordBean {

    @EJB
    private ResetPasswordController resetPasswordController;

    /* The token provided for the password reset request. */
    private String tk;
    /* The email provided for the password reset request. */
    private String ml;
    private String password;
    /* The password reset link was valid. */
    private boolean validRequest;

    public ResetPasswordBean() {
    }

    public String getTk() {
        return tk;
    }

    public void setTk(String tk) {
        this.tk = tk;
    }

    public String getMl() {
        return ml;
    }

    public void setMl(String ml) {
        this.ml = ml;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValidRequest() {
        return validRequest;
    }

    public void setValidRequest(boolean validRequest) {
        this.validRequest = validRequest;
    }

    public void validate() {
        validRequest = resetPasswordController.verifyResetPasswordRequest(tk, ml);
    }

    public String resetPassword() {
        boolean resetSuccessful = resetPasswordController.resetPassword(ml, password, tk);
        if (resetSuccessful) {
            return "resetPasswordSuccess?faces-redirect=true";
        } else {
            return "resetPasswordFailure?faces-redirect=true";
        }
    }
}
