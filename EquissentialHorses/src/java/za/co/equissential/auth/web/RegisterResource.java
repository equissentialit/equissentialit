package za.co.equissential.auth.web;


import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import za.co.equissential.auth.controller.RegisterController;

import za.co.equissential.exceptions.EntityExistsException;
import za.co.equissential.exceptions.SecurityException;

/**
 * Session Bean implementation class PersonResource
 */
@Provider
@Path("register")
public class RegisterResource {

    @EJB
    private RegisterController registerController;

    /**
     * Default constructor.
     */
    public RegisterResource() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRegisterRequest(@Valid RegistrationRequest registrationRequest) throws SecurityException, EntityExistsException {
        registerController.createRegisterRequest(registrationRequest);
        return Response.ok().build();
    }
    
}
