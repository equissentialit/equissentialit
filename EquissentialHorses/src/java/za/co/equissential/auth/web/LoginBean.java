package za.co.equissential.auth.web;

import za.co.equissential.util.SystemBean;
import za.co.equissential.util.BaseBean;
import java.io.IOException;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import za.co.equissential.auth.controller.AuthenticationController;    
import za.co.equissential.person.entity.Person;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.exceptions.SecurityException;
import za.co.equissential.util.PasswordUtil;
import za.co.equissential.util.SessionUtil;

/**
 * The Login bean deals with the EMMA Login screens.
 */
@Named
@RequestScoped
public class LoginBean extends BaseBean {

    @EJB
    private AuthenticationController authenticationController;

    @Inject
    private SystemBean systemBean;

    private Locale locale;

    public LoginBean() {
    }

    @PostConstruct
    public void init() {
    }

    private String email;
    private String password;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SystemBean getSystemBean() {
        return systemBean;
    }

    public void setSystemBean(SystemBean systemBean) {
        this.systemBean = systemBean;
    }

    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            Person person = authenticationController.getPerson(email);
            String storedPassword = person.getPassword();
            String hashedPassword = PasswordUtil.hashPassword(password, storedPassword);

            request.login(email, hashedPassword);
            if (!request.isUserInRole("EquissentialUser")) {
                throw new SecurityException("Invalid role.");
            }
            locale = new Locale("en");
            context.getViewRoot().setLocale(locale);

            HttpSession session = SessionUtil.getSession();
            session.setAttribute("locale", locale);
            session.setAttribute("person", person);

            getSystemBean().init();
        } catch (ServletException | SecurityException | EntityNotFoundException e) {
            context.addMessage("loginError", new FacesMessage(FacesMessage.SEVERITY_WARN, getBundle().getString("loginFailed"), null));
            return null;
        }
        return "/app/dashboard?faces-redirect=true";
    }

    public void logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        try {
            request.logout();
            request.getSession().invalidate();
            externalContext.redirect("/EquissentialHorses");
        } catch (ServletException | IOException e) {
            facesContext.addMessage(null, new FacesMessage("Logout failed."));
        }
    }

    /**
     * Invoked after the register dialog is closed.
     */
    public void registered() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, getBundle().getString("register.register"), getBundle().getString("register.waitForEmail"));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
