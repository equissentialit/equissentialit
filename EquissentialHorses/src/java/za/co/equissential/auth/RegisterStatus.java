package za.co.equissential.auth;

/**
 * Defines the states of a registration request.
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>Pending  - user has submitted an account create request, but has not verified it yet.</li>
 *      <li>Verified - the user has verified their account.</li>
 *      <li>Canceled - the user submitted a new request, which cancels any pending requests.</li>
 *  </ul> 
 */
public enum RegisterStatus {

    VERIFIED("Verified"),
    PENDING("Pending"),
    CANCELLED("Canceled");

    private final String registerStatus;

    RegisterStatus(String registerStatus) {
        this.registerStatus = registerStatus;
    }

    public String value() {
        return registerStatus;
    }  
}
