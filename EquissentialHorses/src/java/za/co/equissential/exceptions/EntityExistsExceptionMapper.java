package za.co.equissential.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityExistsExceptionMapper implements ExceptionMapper<EntityExistsException> {

    @Override
    public Response toResponse(EntityExistsException enaex) {
        return Response.status(409).entity(enaex.getMessage()).type(MediaType.APPLICATION_JSON).build();
    }
}
