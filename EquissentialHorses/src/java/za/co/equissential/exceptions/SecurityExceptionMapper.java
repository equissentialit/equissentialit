package za.co.equissential.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SecurityExceptionMapper implements ExceptionMapper<SecurityException> {

    @Override
    public Response toResponse(SecurityException secex) {
        return Response.status(403).entity(secex.getMessage()).type(MediaType.APPLICATION_JSON).build();
    }
}
