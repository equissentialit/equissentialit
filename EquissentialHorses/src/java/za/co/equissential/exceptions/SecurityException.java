package za.co.equissential.exceptions;

@javax.ejb.ApplicationException(rollback = true)
public class SecurityException extends ApplicationException {

    private static final long serialVersionUID = 3412846069731646387L;

    public SecurityException() {
    }

    public SecurityException(String message) {
        super(message);
    }

    public SecurityException(Throwable cause) {
        super(cause);
    }

    public SecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
