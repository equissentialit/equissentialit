package za.co.equissential.exceptions;

@javax.ejb.ApplicationException(rollback = true)
public class DuplicateEntityException extends ApplicationException {

    private static final long serialVersionUID = 3412846069731646387L;

    public DuplicateEntityException() {
    }

    public DuplicateEntityException(String message) {
        super(message);
    }

    public DuplicateEntityException(Throwable cause) {
        super(cause);
    }

    public DuplicateEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateEntityException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
