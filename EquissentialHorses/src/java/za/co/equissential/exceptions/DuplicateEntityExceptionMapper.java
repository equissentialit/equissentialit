package za.co.equissential.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DuplicateEntityExceptionMapper implements ExceptionMapper<DuplicateEntityException> {

    @Override
    public Response toResponse(DuplicateEntityException dupex) {
        return Response.status(409).entity(dupex.getMessage()).type(MediaType.APPLICATION_JSON).build();
    }
}
