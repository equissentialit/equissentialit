package za.co.equissential.person.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * This class maps a person to a role.
 */
@Entity
@Table(name = "person_role", schema = "equissential")
@NamedQueries({
    @NamedQuery(name = "PersonRole.findAll", query = "SELECT p FROM PersonRole p"),
    @NamedQuery(name = "PersonRole.findByEmail", query = "SELECT p FROM PersonRole p WHERE p.personRolePK.email = :email"),
    @NamedQuery(name = "PersonRole.findByRole", query = "SELECT p FROM PersonRole p WHERE p.personRolePK.role = :role"),
    @NamedQuery(name = "PersonRole.deleteByEmail", query = "DELETE FROM PersonRole p WHERE p.personRolePK.email = :email")})
public class PersonRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonRolePK personRolePK;

    public PersonRole() {
    }

    public PersonRole(PersonRolePK personRolePK) {
        this.personRolePK = personRolePK;
    }

    public PersonRole(String email, String role) {
        this.personRolePK = new PersonRolePK(email, role);
    }

    public PersonRolePK getPersonRolePK() {
        return personRolePK;
    }

    public void setPersonRolePK(PersonRolePK personRolePK) {
        this.personRolePK = personRolePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personRolePK != null ? personRolePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PersonRole)) {
            return false;
        }
        PersonRole other = (PersonRole) object;
        return !((this.personRolePK == null && other.personRolePK != null) || (this.personRolePK != null && !this.personRolePK.equals(other.personRolePK)));
    }

    @Override
    public String toString() {
        return "za.co.equissential.auth.PersonRole[ personRolePK=" + personRolePK + " ]";
    }
    
}
