package za.co.equissential.person.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import za.co.equissential.country.Country;

/**
 * The persistent class for the "ADDRESS" database table.
 */
@Entity
@Table(name = "address", schema = "equissential")
@NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "addressGen")
    @TableGenerator(name = "addressGen",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "ADDRESS_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "Name cannot be null")
    @Size(min = 1, max = 256, message = "Name size must be between 1 and 100 characters")
    @Column(name = "name")
    private String name;
    
    @NotNull(message = "Address cannot be null")
    @Size(min = 1, max = 256, message = "Address size must be between 1 and 256 characters")
    @Column(name = "address")
    private String address;
    
    @NotNull(message = "City cannot be null")
    @Size(min = 1, max = 75, message = "City size must be between 1 and 75 characters")
    @Column(name = "city")
    private String city;

    @NotNull(message = "Country cannot be null")
    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;    

    @NotNull(message = "CountrySubdivision cannot be null")
    @Size(min = 1, max = 75, message = "CountrySubdivision size must be between 1 and 75 characters")
    @Column(name = "country_subdivision")
    private String countrySubdivision;

    @NotNull(message = "PostalCode cannot be null")
    @Size(min = 1, max = 15, message = "PostalCode size must be between 1 and 15 characters")
    @Column(name = "postal_code")
    private String postalCode;
    
    public Address() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCountrySubdivision() {
        return this.countrySubdivision;
    }

    public void setCountrySubdivision(String countrySubdivision) {
        this.countrySubdivision = countrySubdivision;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    
    @Override
    public String toString() {
        return "za.co.cf.emma.entity.Address[ id=" + id + " ]";
    }        

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
