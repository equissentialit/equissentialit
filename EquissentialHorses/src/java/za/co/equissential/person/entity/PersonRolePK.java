package za.co.equissential.person.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author shanebedggood
 */
@Embeddable
public class PersonRolePK implements Serializable {
    
    private static final long serialVersionUID = 1L;    

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "p_role")
    private String role;

    public PersonRolePK() {
    }

    public PersonRolePK(String email, String role) {
        this.email = email;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        hash += (role != null ? role.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PersonRolePK)) {
            return false;
        }
        PersonRolePK other = (PersonRolePK) object;
        if (this.email.equals(other.email)) {
        } else {
            return false;
        }
        return this.role.equals(other.role);
    }

    @Override
    public String toString() {
        return "za.co.equissential.auth.PersonRolePK[ email=" + email + ", role=" + role + " ]";
    }

}
