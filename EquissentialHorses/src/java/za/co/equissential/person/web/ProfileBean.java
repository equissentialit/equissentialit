package za.co.equissential.person.web;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import za.co.equissential.country.Country;
import za.co.equissential.country.CountryController;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.person.controller.PersonController;
import za.co.equissential.person.entity.Address;
import za.co.equissential.person.entity.Person;
import za.co.equissential.util.BaseBean;
import za.co.equissential.util.SessionUtil;


/**
 * The Person bean deals with the EMMA My Profile screens.
 */
@Named
@ViewScoped
public class ProfileBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EJB
    private PersonController personController;
    @EJB
    private CountryController countryController;

    private final Person person;
    private Address postalAddress;
    private Address physicalAddress;
    private List<Country> countries;

    private boolean editing = false;
    private Country selectedCountry;

    public ProfileBean() {
        person = SessionUtil.getLoggedInUser();
    }

    @PostConstruct
    public void init() {
        try {
            countries = countryController.getCountries();
            List<Address> addresses = personController.getAddresses(person.getId());
            if (addresses.isEmpty()) {
                postalAddress = new Address();
                postalAddress.setName("Postal Address");
                physicalAddress = new Address();
                physicalAddress.setName("Physical Address");
            } else {
                addresses.stream().forEach((address) -> {
                    if (address.getName().equals("Postal Address")) {
                        postalAddress = address;
                    } else {
                        if (address.getName().equals("Physical Address")) {
                            physicalAddress = address;
                        }
                    }
                    selectedCountry = address.getCountry();
                });
            }
        } catch (EntityNotFoundException ex) {
            Logger.getLogger(ProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Person getPerson() {
        return person;
    }

    public Address getPostalAddress() {
        return postalAddress;
    }   
    
    public Address getPhysicalAddress() {
        return physicalAddress;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public boolean isEditing() {
        return editing;
    }

    public void edit() {
        editing = true;
    }

    public void cancelBasicInfoEdit() {
        editing = false;
        RequestContext.getCurrentInstance().reset("tabView:personalDetailsForm");
    }

    public void cancelAddressEdit() {
        editing = false;
        RequestContext.getCurrentInstance().reset("tabView:addressForm");
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public void saveBasicInfo() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            personController.updatePerson(person);
            editing = false;
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, MessageFormat.format(getBundle().getString("entityUpdated"), getBundle().getString("basicInfo")), null);
            context.addMessage("feedbackMessages", msg);
        } catch (EntityNotFoundException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFormat.format(getBundle().getString("entityNotUpdated"), getBundle().getString("basicInfo")), null);
            context.addMessage("feedbackMessages", msg);
        }
    }

    public void savePostalAddress() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (postalAddress.getId() == null) {
                personController.addAddress(person.getId(), postalAddress);
                FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityAdded"), getBundle().getString("address")), null);
                context.addMessage("feedbackMessages", msg);
            } else {
                personController.updateAddress(person.getId(), postalAddress);
                FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityUpdated"), getBundle().getString("address")), null);
                context.addMessage("feedbackMessages", msg);
            }
            editing = false;
        } catch (EntityNotFoundException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFormat.format(getBundle().getString("entityNotUpdated"), getBundle().getString("address")), null);
            context.addMessage("feedbackMessages", msg);
        }
    }
    
    public void savePhysicalAddress() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (postalAddress.getId() == null) {
                personController.addAddress(person.getId(), physicalAddress);
                FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityAdded"), getBundle().getString("address")), null);
                context.addMessage("feedbackMessages", msg);
            } else {
                personController.updateAddress(person.getId(), physicalAddress);
                FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityUpdated"), getBundle().getString("address")), null);
                context.addMessage("feedbackMessages", msg);
            }
            editing = false;
        } catch (EntityNotFoundException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFormat.format(getBundle().getString("entityNotUpdated"), getBundle().getString("address")), null);
            context.addMessage("feedbackMessages", msg);
        }
    }
}
