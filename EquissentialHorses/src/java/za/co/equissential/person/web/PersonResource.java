package za.co.equissential.person.web;

import java.util.List;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import za.co.equissential.auth.controller.ResetPasswordController;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.person.controller.PersonController;
import za.co.equissential.person.entity.Address;
import za.co.equissential.person.entity.Person;

/**
 * Session Bean implementation class PersonResource
 */
@Provider
@Path("persons")
public class PersonResource {

    @EJB
    private PersonController personController;
    
    @EJB
    private ResetPasswordController resetPasswordController;

    /**
     * Default constructor.
     */
    public PersonResource() {
    }

    @GET
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPerson(@PathParam("personId") long personId) throws EntityNotFoundException {
        return personController.getPerson(personId);
    }
    
    @GET
    @Path("email/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPersonByEmail(@PathParam("email") String email) throws EntityNotFoundException {
        return personController.getPersonByEmail(email);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePerson(Person person) throws EntityNotFoundException {
        personController.updatePerson(person);
        return Response.ok().build();
    }

    @POST
    @Path("{personId}/addresses")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAddress(@PathParam("personId") long personId, @Valid Address address) throws EntityNotFoundException {
        personController.addAddress(personId, address);
        return Response.status(Response.Status.CREATED).build();
    }

    /**
     * Update the specified address for the specified person with the address provided.
     * 
     * @param personId The person whose address must be updated.
     * @param addressId The address to update.
     * @param address The updated address.
     * @return HTTP 200, 400, 403, 404.
     * @throws EntityNotFoundException if the person or address cannot be found.
     */
    @PUT
    @Path("{personId}/addresses/{addressId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAddress(@PathParam("personId") long personId, @PathParam("addressId") long addressId, Address address) throws EntityNotFoundException {
        personController.updateAddress(personId, address);
        return Response.ok().build();
    }

    @DELETE
    @Path("{personId}/addresses/{addressId}")
    public Response deleteAddress(@PathParam("personId") long personId, @PathParam("addressId") long addressId) throws EntityNotFoundException {
        personController.deleteAddress(personId, addressId);
        return Response.ok().build();
    }

    @GET
    @Path("{personId}/addresses/{addressId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Address getAddress(@PathParam("personId") long personId, @PathParam("addressId") long addressId) throws EntityNotFoundException {
        return personController.getAddress(personId, addressId);
    }

    @GET
    @Path("{personId}/addresses")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Address> getAddresses(@PathParam("personId") long personId) throws EntityNotFoundException {
        return personController.getAddresses(personId);
    }
    
//    @PUT
//    @Path("password")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response resetPassword(@Valid ResetCredential resetPassword) throws EntityNotFoundException, SecurityException {
//        resetPasswordController.processPasswordChangeRequest(resetPassword.getEmail());
//        return Response.accepted().build();
//    }
}
