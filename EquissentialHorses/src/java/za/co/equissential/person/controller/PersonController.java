package za.co.equissential.person.controller;

import java.util.List;
import javax.annotation.security.RolesAllowed;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import za.co.equissential.country.Country;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.person.entity.Address;
import za.co.equissential.person.entity.Person;
import za.co.equissential.person.entity.PersonRole;
import za.co.equissential.person.entity.PersonRolePK;


/**
 * Session Bean implementation class PersonController
 */
@Stateless
@LocalBean
@RolesAllowed({"EquissentialUser"})
public class PersonController {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Default constructor.
     */
    public PersonController() {
    }

    public void setEntityManager(EntityManager em) {
        this.entityManager = em;
    }

    /**
     * Find a person by email.
     *
     * @param email the email of the person to find
     * @return a person
     * @throws za.co.equissential.exceptions.EntityNotFoundException if the person with the specified email cannot be found
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Person getPersonByEmail(String email) throws EntityNotFoundException {
        Query query = entityManager.createNamedQuery("Person.findByEmail");
        query.setParameter("email", email);
        try {
            return (Person) query.getSingleResult();
        }
        catch (NoResultException noResultException) {
            throw new EntityNotFoundException("Entity " + Person.class.getSimpleName() + " with id " + email + " not found.");
        }
    }
    
        /**
     * Find a person by email.
     *
     * @param name the name of the person to find
     * @param surname the surname of the person to find
     * @return a person and null if person doesn't exist
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Person getPersonByNameSurname(String name, String surname) {
        Query query = entityManager.createNamedQuery("Person.findByNameSurname");
        query.setParameter("name", name);
        query.setParameter("surname", surname);
        try {
            return (Person) query.getSingleResult();
        }
        catch (NoResultException noResultException) {
//            throw new EntityNotFoundException("Entity " + Person.class.getSimpleName() + " with name " + name + " " + surname + "not found.");
            return null;
        }
    }

    /**
     * Update an exiting Person.
     *
     * @param person the latest person details
     * @throws za.co.equissential.exceptions.EntityNotFoundException if the person cannot be found
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updatePerson(@Valid Person person) throws EntityNotFoundException {
        if (person.getId() == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + person.getId() + " not found.");
        }

        Person storedPerson = entityManager.find(Person.class, person.getId());
        if (storedPerson == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + person.getId() + " not found.");
        }
        
        updateRoles(storedPerson, person);
        
        storedPerson.setEmail(person.getEmail());
        storedPerson.setFirstName(person.getFirstName());
        storedPerson.setSurname(person.getSurname());
        storedPerson.setMobileNumber(person.getMobileNumber());
        storedPerson.setPhoneNumber(person.getPhoneNumber());
        entityManager.merge(storedPerson);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createPerson(Person person) {
        entityManager.persist(person);
    }

    /**
     * Create a new address.
     *
     * @param personId the id of the person to add the address to
     * @param address the address to add
     * @throws EntityNotFoundException when an address is not found for the provided personId
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addAddress(long personId, @Valid Address     address) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        
        entityManager.persist(address);
        person.getAddresses().add(address);
        entityManager.merge(person);
    }

    /**
     * Update an address.
     *
     * @param personId the person that owns the address
     * @param addressId the address to update
     * @param address the address to update
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateAddress(long personId, @Valid Address address) throws EntityNotFoundException {
        Address storedAddress = entityManager.find(Address.class, address.getId());
        if (storedAddress == null) {
            throw new EntityNotFoundException(
                    "Entity " + Address.class.getSimpleName() + " with id " + address.getId() + " not found.");
        }
        
        storedAddress.setAddress(address.getAddress());
        storedAddress.setCity(address.getCity());        
        if (!storedAddress.getCountry().getName().equals(address.getCountry().getName())) {
            Country country = entityManager.find(Country.class, address.getCountry().getId());
            storedAddress.setCountry(country);
        }        
        storedAddress.setCountrySubdivision(address.getCountrySubdivision());
        storedAddress.setPostalCode(address.getPostalCode());
        entityManager.merge(storedAddress);
    }

    /**
     * Delete the specified address.
     *
     * @param personId the person that owns the address
     * @param addressId the address to delete
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteAddress(long personId, long addressId) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        
        Address address = entityManager.find(Address.class, addressId);
        if (address == null) {
            throw new EntityNotFoundException(
                    "Entity " + Address.class.getSimpleName() + " with id " + addressId + " not found.");
        }
        
        person.getAddresses().remove(address);
        entityManager.remove(address);
    }

    /**
     * Get the specified address.
     *
     * @param personId the person that owns the address
     * @param addressId the address to get
     * @return the specified address
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Address getAddress(long personId, long addressId) throws EntityNotFoundException {
        Address address = entityManager.find(Address.class, addressId);
        if (address == null) {
            throw new EntityNotFoundException(
                    "Entity " + Address.class.getSimpleName() + " with id " + addressId + " not found.");
        }
        return address;
    }

    /**
     * Get all addresses for the specified person.
     *
     * @param personId the person to get the addresses for
     * @return a list of AddressTOs for the specified person
     * @throws EntityNotFoundException when a person is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Address> getAddresses(long personId) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        return person.getAddresses();
    }

    /**
     * Get the specified person.
     *
     * @param personId the person to get
     * @return the specified person
     * @throws EntityNotFoundException when a person is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Person getPerson(long personId) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        return person;
    }

    /**
     * Create new roles and remove existing roles when a person updates their email address.
     * 
     * @param storedPerson The person currently stored.
     * @param person The updated person.
     */
    private void updateRoles(Person storedPerson, Person person) {
        if (!storedPerson.getEmail().equals(person.getEmail())) {
            Query query = entityManager.createNamedQuery("PersonRole.findByEmail");
            query.setParameter("email", storedPerson.getEmail());
            List<PersonRole> personRoles = (List<PersonRole>) query.getResultList();
            for (PersonRole personRole : personRoles) {
                PersonRolePK personRolePK = new PersonRolePK(person.getEmail(), personRole.getPersonRolePK().getRole());
                PersonRole newPersonRole = new PersonRole(personRolePK);
                entityManager.persist(newPersonRole);
            }
            query = entityManager.createNamedQuery("PersonRole.deleteByEmail");
            query.setParameter("email", storedPerson.getEmail());
            query.executeUpdate();
        }
    }

}
