/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.organisation;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import za.co.equissential.person.entity.Person;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "organisation_member", schema = "equissential")
public class OrganisationMembership implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "OrganisationMembershipGen")
    @TableGenerator(name = "OrganisationMembershipGen",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "ORGANISATION_MEMBER_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @NotNull(message = "Membership number cannot be null")
    @Column(name = "membership_number")
    private String membershipNumber;

    @ManyToOne
    @JoinColumn(name = "organisation_id", referencedColumnName = "id")
    private Organisation organisation;
    
    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person member;
   
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "organisation_member_id", referencedColumnName = "id")
    private List<HorseMembership> horseMemberships;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "organisation_member_id", referencedColumnName = "id")
    private List<OrganisationMemberRole> organisationRoles;
    
    
//    @OneToOne(fetch = FetchType.LAZY)
//    private MembershipType membershipType;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganisationMembership)) {
            return false;
        }
        OrganisationMembership other = (OrganisationMembership) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.co.equissential.organisation.OrganisationMembership[ id=" + id + " ]";
    }

    /**
     * @return the membershipNumber
     */
    public String getMembershipNumber() {
        return membershipNumber;
    }

    /**
     * @param membershipNumber the membershipNumber to set
     */
    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    /**
     * @return the organisation
     */
    public Organisation getOrganisation() {
        return organisation;
    }

    /**
     * @param organisation the organisation to set
     */
    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    /**
     * @return the member
     */
    public Person getMember() {
        return member;
    }

    /**
     * @param member the member to set
     */
    public void setMember(Person member) {
        this.member = member;
    }

    /**
     * @return the horseMemberships
     */
    public List<HorseMembership> getHorseMemberships() {
        return horseMemberships;
    }

    /**
     * @param horseMemberships the horseMemberships to set
     */
    public void setHorseMemberships(List<HorseMembership> horseMemberships) {
        this.horseMemberships = horseMemberships;
    }

    /**
     * @return the organisationRoles
     */
    public List<OrganisationMemberRole> getOrganisationRoles() {
        return organisationRoles;
    }

    /**
     * @param organisationRoles the organisationRoles to set
     */
    public void setOrganisationRoles(List<OrganisationMemberRole> organisationRoles) {
        this.organisationRoles = organisationRoles;
    }
    
}
