/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.organisation;

/**
 * Defines the roles of an organisation
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>Administrator - user can administer the specific organisation</li>
 *      <li>Organisation Owner - user is the owner of a specific organisation</li>
 *      <li>Horse Owner - user is the owner of a horse registered with the organisation</li>
 *      <li>Member - user is a member of the organisation</li>
 *      <li>Instructor - user is an instructor for the specific organisation</li>
 *      <li>Judge - user is a judge for the organisation</li>
 *      <li>Groom - user is a groom for the organisation</li>
 *  </ul> 
 */
public enum OrganisationRole {
    ADMINISTRATOR("Administrator"), 
    ORGANISATION_OWNER("Organisation Owner"), 
    HORSE_OWNER("Horse Owner"), 
    MEMBER("Member"), 
    INSTRUCTOR("Instructor"), 
    JUDGE("Judge"), 
    GROOM("Groom");

    private final String organisationRole;

    OrganisationRole(String organisationRole) {
        this.organisationRole = organisationRole;
    }

    public String value() {
        return organisationRole;
    }  
    
}
