/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.organisation;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "organisation_member_horse", schema = "equissential")
public class HorseMembership implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "horseMembershipGen")
    @TableGenerator(name = "horseMembershipGen",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "ORGANISATION_MEMBER_HORSE_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "horse_membership_number")
    private String membershipNumber;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorseMembership)) {
            return false;
        }
        HorseMembership other = (HorseMembership) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.co.equissential.entity.HorseMembership[ id=" + id + " ]";
    }

    /**
     * @return the membershipNumber
     */
    public String getMembershipNumber() {
        return membershipNumber;
    }

    /**
     * @param membershipNumber the membershipNumber to set
     */
    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = membershipNumber;
    }
    
}
