/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.organisation;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "organisation_member_role", schema = "equissential")
public class OrganisationMemberRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "OrganisationMemberRoleGen")
    @TableGenerator(name = "OrganisationMemberRoleGen",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "ORGANISATION_MEMBER_ROLE_SEQ",
                    allocationSize=1,
                    initialValue=0)
    private Long id;
    
    @NotNull(message = "Organisation role cannot be null")
    @Column(name = "organisation_role")
    private String organisationRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganisationMemberRole)) {
            return false;
        }
        OrganisationMemberRole other = (OrganisationMemberRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.co.equissential.organisation.OrganisationMemberRole[ id=" + id + " ]";
    }

    /**
     * @return the organisationRole
     */
    public String getOrganisationRole() {
        return organisationRole;
    }

    /**
     * @param organisationRole the organisationRole to set
     */
    public void setOrganisationRole(String organisationRole) {
        this.organisationRole = organisationRole;
    }
    
}
