package za.co.equissential.country;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the "COUNTRY" database table.
 */
@Entity
@Table(name = "country", schema = "equissential")
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull(message = "Country name cannot be null")
    @Column(name = "country_name")
    private String name;

    @NotNull(message = "Alpha2 cannot be null")
    @Column(name = "alpha_2")
    private String alpha2;

    @NotNull(message = "Alpha3 cannot be null")
    @Column(name = "alpha_3")
    private String alpha3;    
    
    public Country() {
    }
    
    public Country(String name, String alpha2, String alpha3) {
        this.name = name;
        this.alpha2 = alpha2;
        this.alpha3 = alpha3;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlpha2() {
        return alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    public String getAlpha3() {
        return alpha3;
    }

    public void setAlpha3(String alpha3) {
        this.alpha3 = alpha3;
    }

    @Override
    public String toString() {
        return "za.co.cf.emma.entity.Country[ id=" + id + " ]";
    }        
}