package za.co.equissential.country;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Provider
@Path("countries")
public class CountryResource {
    
    @EJB
    private CountryController countryController;

    /**
     * Default constructor.
     */
    public CountryResource() {
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Country> getCountries() {
        return countryController.getCountries();
    }
}
