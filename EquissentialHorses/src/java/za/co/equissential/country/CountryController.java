package za.co.equissential.country;

import java.util.List;
import javax.annotation.security.RolesAllowed;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Session Bean implementation class PersonController
 */
@Stateless
@LocalBean
@RolesAllowed({"EquissentialUser"})
public class CountryController {

    @PersistenceContext
    private EntityManager em;

    /**
     * Default constructor.
     */
    public CountryController() {
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    /**
     * Get all countries.
     *
     * @return a list of CountryTOs
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Country> getCountries() {
        Query query = em.createQuery("SELECT c FROM Country c");
        return query.getResultList();
    }

}
