/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.shows.controller.ShowController;
import za.co.equissential.shows.controller.ShowEntryHelper;
import za.co.equissential.shows.entity.Show;
import za.co.equissential.shows.util.ShowScheduleTO;

/**
 * Session Bean implementation class ShowResource
 */
@Provider
@Path("organisations")
public class ShowResource {
    
    @EJB
    private ShowController showController;
    
    public ShowResource() {
        
    }
    
    @GET
    @Path("{organisationId}/shows")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Show> getShows(@PathParam("organisationId") long organisationId) throws EntityNotFoundException {
        return showController.getShows(organisationId);
    }

    @POST
    @Path("{organisaionId}/shows/{showId}/entries")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addEntries(@PathParam("organisationId") long organisationId, @PathParam("showId") long showId, ArrayList<ShowEntryHelper> entries) throws EntityNotFoundException {
         showController.addShowEntries(organisationId, showId, entries);
         return Response.status(Response.Status.CREATED).build();
    }
    
    @GET
    @Path("{organisaionId}/shows/{showId}/schedule")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * returns a map of entry clashes pairs, if any.
     */
    public HashMap<Long, Long> createShowSchedule(@PathParam("organisationId") long organisationId, @PathParam("showId") long showId) throws EntityNotFoundException {
        return showController.createShowSchedule(showId);
    }
}
