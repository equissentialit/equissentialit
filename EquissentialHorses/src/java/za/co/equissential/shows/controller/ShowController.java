/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.exceptions.ApplicationException;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.horse.controller.HorseController;
import za.co.equissential.horse.entity.Horse;
import za.co.equissential.person.controller.PersonController;
import za.co.equissential.person.entity.Person;
import za.co.equissential.shows.entity.ShowEntry;
import za.co.equissential.shows.entity.Show;
import za.co.equissential.shows.entity.ShowClass;
import za.co.equissential.shows.entity.ShowProgram;
import za.co.equissential.shows.util.ShowClassComparator;
import za.co.equissential.shows.util.ShowEntryRunningOrderComparator;
import za.co.equissential.shows.util.ShowEntryTimeComparator;
import za.co.equissential.shows.util.ShowProgramComparator;
import za.co.equissential.shows.util.ShowScheduleTO;
import za.co.equissential.util.TOUtil;

@Stateless
@LocalBean
@RolesAllowed({"EquissentialUser"})
public class ShowController {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private PersonController personController;

    @EJB
    private HorseController horseController;

    private final int entryTimeLapse = 14;

    /**
     * Default constructor.
     */
    public ShowController() {
    }

    public void setEntityManager(EntityManager em) {
        this.entityManager = em;
    }

    @RolesAllowed("EquissentialUser")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Show> getShows(long organisationId) {
        Query query = entityManager.createNamedQuery("Show.findShowsByOrganisationId");
        query.setParameter("organisationId", organisationId);
        return (List<Show>) query.getResultList();
    }

    @RolesAllowed("EquissentialUser")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<ShowProgram> getShowPrograms(long showId) {
        Show show = entityManager.find(Show.class, showId);
        return show.getShowPrograms();
    }

    @RolesAllowed("EquissentialUser")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public HashMap<Long, Long> createShowSchedule(long showId) throws ApplicationException {
        Show show = findShowById(showId);
        List<ShowProgram> showPrograms = show.getShowPrograms();
        showPrograms.sort(new ShowProgramComparator());
        for (ShowProgram program : showPrograms) {
            if (program.isScheduleCompleted()) {
                continue;
            }
            LocalTime nextClassStartTime = program.getStartTime();
            List<ShowClass> classes = program.getClassesForArena();
            classes.sort(new ShowClassComparator());
            for (ShowClass showClass : classes) {
                List<ShowEntry> showEntries = showClass.getShowEntries();
                if (showEntries.isEmpty()) {
                    continue;
                }
                showClass.setApproximateStartTime(nextClassStartTime);
                LocalTime entryStartTime = nextClassStartTime.plusMinutes(showClass.getClassType().getCourseWalkInMinutes());
                nextClassStartTime = entryStartTime;
                sortEntries(showClass, showEntries);
                int showEntryNr = 1;
                int followOnEntryNr = 1;
                for (ShowEntry showEntry : showEntries) {
                    long timePerCompetitor = showClass.getClassType().getTimePerCompetitor();
                    showEntryNr = setRunningOrder(showClass, showEntry, showEntryNr);
                    if (followOnEntryNr != showEntryNr) {
                        followOnEntryNr = setRunningOrderOnFollowOnClassEntry(followOnEntryNr, showClass.getFollowOnClass(), showEntry);
                    } else {
                        followOnEntryNr = setRunningOrderOnFollowOnClassEntry(showEntryNr, showClass.getFollowOnClass(), showEntry);
                    }
                    /*
                        if class has follow on class, find entry in follow on class for same competitor and horse
                            and set same runningOrder
                     */
                    showEntry.setApproximateStartTime(entryStartTime);
                    entryStartTime = entryStartTime.plusMinutes(timePerCompetitor);
                    showEntryNr++;
                    followOnEntryNr++;
                    nextClassStartTime = nextClassStartTime.plusMinutes(timePerCompetitor);
                    entityManager.merge(showEntry);
                }
                nextClassStartTime = nextClassStartTime.plusMinutes(showClass.getClassType().getTimeForPrizegiving());
            }
            entityManager.merge(program);
        }
        entityManager.merge(show);

        return checkRidingOrderForRiderClashes(show);
    }
    
    /**
     * 
     * @param runningOrder the current entry's running order
     * @param followOnClass the class where to find the follow on entry
     * @param currentEntry the entry from where to find rider and horse for follow on
     * @return the follow on running order
     * 
     * if class has follow on class, find entry in follow on class for same competitor and horse
     *  and set same runningOrder
     */
    private int setRunningOrderOnFollowOnClassEntry(int runningOrder, ShowClass followOnClass, ShowEntry currentEntry) {
        int followOnOrder = runningOrder;
        if (followOnClass != null) {
            ShowEntry followOnEntry = findShowEntryForClassRiderHorse(followOnClass, currentEntry.getRider(), currentEntry.getHorse());
            if (followOnEntry != null) {
                followOnEntry.setRunningOrder(runningOrder);
                entityManager.merge(followOnEntry);
            } else {
                followOnOrder--;
            }
        }
        return followOnOrder;
    }

    /**
     * 
     * @param showClass
     * @param showEntries 
     * 
     * if class is follow on class, order entries else shuffle entries
     */
    private void sortEntries(ShowClass showClass, List<ShowEntry> showEntries) {
        if (showClass.isIsFollowOn()) {
            Collections.sort(showEntries, new ShowEntryRunningOrderComparator());
        } else {
            Collections.shuffle(showEntries);
        }
    }

    /**
     * 
     * @param showClass
     * @param showEntry
     * @param showEntryNr for next entry
     * 
     * if class is not a follow on, set running order, otherwise, if class is a follow on class 
     *  and running order is not 0, get running order and set as next showEntryNr, 
     * if running order is not set, set it to next entry number.
     */
    private int setRunningOrder(ShowClass showClass, ShowEntry showEntry, int showEntryNr) {
        if (!showClass.isIsFollowOn()) {
            showEntry.setRunningOrder(showEntryNr);
        } else {
            if (showEntry.getRunningOrder() != 0) {
                showEntryNr = showEntry.getRunningOrder();
            } else {
                showEntry.setRunningOrder(showEntryNr);
            }
        }
        return showEntryNr;
    }

    private HashMap<Long, Long> checkRidingOrderForRiderClashes(Show show) {
        ArrayList<LocalDate> showDates = show.getAllShowDates();
        HashMap<Long, Long> entryClashes = new HashMap<>();
        showDates.forEach(showDate -> {
            List<ShowProgram> showPrograms = findShowProgramsForShowDate(show, showDate);
            HashMap<String, List<ShowEntry>> classEntriesMap = new HashMap<>();
            HashMap<Person, List<ShowEntry>> personEntries = new HashMap<>();
            showPrograms.forEach(program -> {
                program.getClassesForArena().forEach(showClass -> {
                    classEntriesMap.put(showClass.getCode(), showClass.getShowEntries());
                    showClass.getShowEntries().forEach(showEntry -> {
                        List<ShowEntry> entries = personEntries.get(showEntry.getRider());
                        if (entries == null) {
                            entries = new ArrayList<>();
                        }
                        entries.add(showEntry);
                        personEntries.put(showEntry.getRider(), entries);
                    });
                });
            });

            personEntries.keySet().forEach(rider -> {
                entryClashes.putAll(checkAndFixEntryTimesPerRider(rider, personEntries));
            });
        });
        return entryClashes;
    }

    private HashMap<Long, Long> checkAndFixEntryTimesPerRider(Person rider, HashMap<Person, List<ShowEntry>> personEntries) {

        HashMap<Long, Long> entryClashesIds = new HashMap<>();
        List<ShowEntry> entryList = personEntries.get(rider);
        Collections.sort(entryList, new ShowEntryTimeComparator());
        LocalTime entryTime;
        ShowEntry prevEntry = null;
        for (ShowEntry entry : entryList) {
            entryTime = entry.getApproximateStartTime();
            if (prevEntry != null) {
                //Check if current entry is <entryTimeLapse> minutes or more after previous entry, 
                if (!prevEntry.getApproximateStartTime().isBefore(entryTime.minusMinutes(entryTimeLapse))) {
                    //Find entry of same class that is more than 15 mins after current entry and swop
                    ShowEntry entryToSwop = findShowEntryForClassAfterTime(entry.getShowClass(), prevEntry.getApproximateStartTime().plusMinutes(entryTimeLapse + 2));
                    if (entryToSwop != null) {
                        swopShowEntries(entry, entryToSwop);
                    } else {
                        //if that entry doesn't exist, get previous entry and find entry of same class 15 mins before prev entry and swop
                        entryToSwop = findShowEntryForClassBeforeTime(prevEntry.getShowClass(), entry.getApproximateStartTime().minusMinutes(entryTimeLapse + 2));
                        if (entryToSwop != null) {
                            swopShowEntries(prevEntry, entryToSwop);
                        } else {
                            //if that can't be found, then flag as problem entry.
                            entryClashesIds.put(prevEntry.getId(), entry.getId());
                        }
                    }
                }
            }
            prevEntry = entry;
        }
        return entryClashesIds;
    }

    private void swopShowEntries(ShowEntry entry1, ShowEntry entry2) {
        int entryOrder1 = entry1.getRunningOrder();
        LocalTime time1 = entry1.getApproximateStartTime();
        int entryOrder2 = entry2.getRunningOrder();
        LocalTime time2 = entry2.getApproximateStartTime();
        entry1.setApproximateStartTime(time2);
        entry1.setRunningOrder(entryOrder2);
        entry1.setEntrySwopped(true);
        entry2.setApproximateStartTime(time1);
        entry2.setRunningOrder(entryOrder1);
        entry2.setEntrySwopped(true);
        entityManager.merge(entry1);
        entityManager.merge(entry2);
    }

    /**
     * Find show entry after the given time that hasn't been swopped already
     */
    private ShowEntry findShowEntryForClassAfterTime(ShowClass showClass, LocalTime time) {
        Query query = entityManager.createNamedQuery("ShowEntry.findShowEntryForClassAfterTime");
        query.setParameter("showClassId", showClass.getId());
        query.setParameter("time", time);
        List<ShowEntry> entryList = query.getResultList();
        ShowEntry entry = null;
        if (!entryList.isEmpty()) {
            // Get the first entry for the specified criteria
            entry = entryList.get(0);
        }
        return entry;
    }
    
    private ShowEntry findShowEntryForClassRiderHorse(ShowClass showClass, Person rider, Horse horse) {
        Query query = entityManager.createNamedQuery("ShowEntry.findShowEntryForClassRiderHorse");
        query.setParameter("showClass", showClass);
        query.setParameter("rider", rider);
        query.setParameter("horse", horse);
        ShowEntry entry = null;
        try {
            entry = (ShowEntry) query.getSingleResult();
        } catch (NoResultException r) {
        }
        return entry;
    }

    /**
     * Find show entry after the given time that hasn't been swopped already
     */
    private ShowEntry findShowEntryForClassBeforeTime(ShowClass showClass, LocalTime time) {
        Query query = entityManager.createNamedQuery("ShowEntry.findShowEntryForClassBeforeTime");
        query.setParameter("showClassId", showClass.getId());
        query.setParameter("time", time);
        List<ShowEntry> entryList = query.getResultList();
        ShowEntry entry = null;
        if (!entryList.isEmpty()) {
            // Get the first entry for the specified criteria
            entry = entryList.get(0);
        }
        return entry;
    }

    @RolesAllowed("EquissentialUser")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addShowEntries(long organisationId, long showId, ArrayList<ShowEntryHelper> entries) throws EntityNotFoundException {
        Show show = findShowById(showId);

        HashMap<String, ShowClass> classMap = new HashMap<>();
        HashMap<String, Person> riderMap = new HashMap<>();
        HashMap<String, Horse> horseMap = new HashMap<>();
        Collections.sort(entries);
        for (ShowEntryHelper entryHelper : entries) {
            ShowClass showClass = findShowClassForShow(entryHelper.getShowClassCode(), show, classMap);
            System.out.println("Class:  " + entryHelper.getShowClassCode());
            if (showClass == null) {
                throw new EntityNotFoundException("The show class " + entryHelper.getShowClassCode() + " for show " + show);
            }
            Person rider = getRider(entryHelper.getRiderName(), entryHelper.getRiderSurname(), riderMap);
            System.out.println("Rider:  " + rider.getFirstName() + " " + rider.getSurname());
            Horse horse = getHorse(entryHelper.getHorseName(), horseMap, rider);
            ShowEntry entry = new ShowEntry();
            entry.setHorse(horse);
            entry.setRider(rider);
            entry.setSchoolName(entryHelper.getSchoolName());
            entry.setShowClass(showClass);
            showClass.addShowEntry(entry);

        }

        classMap.values().forEach((showClass) -> {
            addOrUpdateShowClass(showClass);
        });
    }

    private Show findShowById(long showId) {
        return entityManager.find(Show.class, showId);
    }

    private Person getRider(String riderName, String riderSurname, HashMap<String, Person> riderMap) throws EntityNotFoundException {
        String riderFullname = riderName + " " + riderSurname;
        Person rider = riderMap.get(riderFullname);
        if (rider == null) {
            rider = personController.getPersonByNameSurname(riderName, riderSurname);
            if (rider == null) {
                rider = new Person();
                rider.setFirstName(riderName);
                rider.setSurname(riderSurname);
                personController.createPerson(rider);
            }
            riderMap.put(riderFullname, rider);
        }
        return rider;
    }

    private Horse getHorse(String horseName, HashMap<String, Horse> horseMap, Person person) throws EntityNotFoundException {
        Horse horse = horseMap.get(horseName);
        if (horse == null) {
            horse = horseController.findHorseByName(horseName);
            if (horse == null) {
                horse = new Horse();
                horse.setName(horseName);
                horse.setId(horseController.addHorse(person.getId(), horse));
            }
            horseMap.put(horseName, horse);
        }
        return horse;
    }

    public List<Person> findEntrantsForShowDate(Show show, LocalDate showDate) {
        Query query = entityManager.createNamedQuery("Show.findEntrantsForShowDate");
        query.setParameter("name", show.getShowName());
        query.setParameter("date", showDate);
        return (List<Person>) query.getResultList();
    }

    private Show findShowByNameDate(String showName, LocalDate date) throws EntityNotFoundException {
        Query query = entityManager.createNamedQuery("Show.findByNameAndDate");
        query.setParameter("name", showName);
        query.setParameter("date", date);
        try {
            return (Show) query.getSingleResult();
        } catch (NoResultException noResultException) {
            throw new EntityNotFoundException("Entity " + Show.class.getSimpleName() + " with name " + showName + " not found.");
        }
    }

    private ShowClass findShowClass(String code, Show show) throws EntityNotFoundException {
        List<ShowProgram> programs = show.getShowPrograms();
        ShowClass chosen = null;
        for (ShowProgram program : programs) {
            List<ShowClass> classes = program.getClassesForArena();
            for (ShowClass showClass : classes) {
                if (showClass.getCode().equals(code)) {
                    chosen = showClass;
                    break;
                }
            }
        }

        return chosen;

    }

    private void addOrUpdateShowClass(ShowClass showClass) {
        if (showClass.getId() == null) {
            entityManager.persist(showClass);
        } else {
            entityManager.merge(showClass);
        }
    }

    private long saveShowEntry(ShowEntry entry) {
        if (entry.getId() == null) {
            entityManager.persist(entry);
        } else {
            entityManager.merge(entry);
        }
        return entry.getId();
    }

    private ShowClass findShowClassForShow(String classCode, Show show, HashMap<String, ShowClass> classMap) throws EntityNotFoundException {
        ShowClass showClass = classMap.get(classCode);
        if (showClass == null) {
            showClass = findShowClass(classCode, show);
            classMap.put(classCode, showClass);
        }
        return showClass;
    }

    private List<ShowEntry> findEntriesForEntrantDay(LocalDate showDate, Person entrant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private List<ShowProgram> findShowProgramsForShowDate(Show show, LocalDate showDate) {
        Query query = entityManager.createNamedQuery("Show.findShowProgramsForDate");
        query.setParameter("showId", show.getId());
        query.setParameter("date", showDate);
        return (List<ShowProgram>) query.getResultList();
    }

}
