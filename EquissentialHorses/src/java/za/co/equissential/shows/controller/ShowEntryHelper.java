/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.controller;

import java.io.Serializable;

/**
 *
 * @author liezelbedggood
 */
public class ShowEntryHelper implements Serializable, Comparable<ShowEntryHelper>{
    private String showClassCode;
    private String riderName;
    private String riderSurname;
    private String horseName;
    private String schoolName;

    /**
     * @return the riderName
     */
    public String getRiderName() {
        return riderName;
    }

    /**
     * @param riderName the riderName to set
     */
    public void setRiderName(String riderName) {
        this.riderName = riderName.trim();
    }

    /**
     * @return the riderSurname
     */
    public String getRiderSurname() {
        return riderSurname;
    }

    /**
     * @param riderSurname the riderSurname to set
     */
    public void setRiderSurname(String riderSurname) {
        this.riderSurname = riderSurname.trim();
    }

    /**
     * @return the horseName
     */
    public String getHorseName() {
        return horseName;
    }

    /**
     * @param horseName the horseName to set
     */
    public void setHorseName(String horseName) {
        this.horseName = horseName.trim();
    }

    /**
     * @return the schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * @param schoolName the schoolName to set
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName.trim();
    }

    /**
     * @return the showClassCode
     */
    public String getShowClassCode() {
        return showClassCode;
    }

    /**
     * @param showClassCode the showClassCode to set
     */
    public void setShowClassCode(String showClassCode) {
        this.showClassCode = showClassCode.trim();
    }

    @Override
    public int compareTo(ShowEntryHelper entryHelper) {
        int compare = showClassCode.compareTo(entryHelper.getShowClassCode());
        if (compare == 0) {
            compare = riderName.compareTo(entryHelper.getRiderName());
            if (compare == 0) {
                compare = riderSurname.compareTo(entryHelper.getRiderSurname());
                if (compare == 0) {
                    compare = horseName.compareTo(entryHelper.getHorseName());
                }
            }
        } 
        return compare;
    }
}
