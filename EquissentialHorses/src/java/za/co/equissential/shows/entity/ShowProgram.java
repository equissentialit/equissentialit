/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "show_program", schema = "equissential")
@NamedQueries( {
    
})
public class ShowProgram implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "showDayArenaGen")
    @TableGenerator(name = "showDayArenaGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "SHOW_PROGRAM_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "arena_id", referencedColumnName = "id")
    private Arena arena;
    
    @Column(name = "show_date")
    @Temporal(TemporalType.DATE)
    private LocalDate date;
    
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private LocalTime startTime;
    
    @Column(name = "arena_has_preference")
    private boolean arenaHasPreference;

    @Column(name = "schedule_completed")
    private boolean scheduleCompleted;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "show_program_id", referencedColumnName = "id")
    private List<BreakTime> breakTimes = new ArrayList<>();
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "show_program_id", referencedColumnName = "id")
    private List<ShowClass> classesForArena = new ArrayList<>();

    /**
     * @return the arena
     */
    public Arena getArena() {
        return arena;
    }

    /**
     * @param arena the arena to set
     */
    public void setArena(Arena arena) {
        this.arena = arena;
    }

    /**
     * @return the startTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the classesForArena
     */
    public List<ShowClass> getClassesForArena() {
        return classesForArena;
    }

    /**
     * @param classesForArena the classesForArena to set
     */
    public void setClassesForArena(List<ShowClass> classesForArena) {
        this.classesForArena = classesForArena;
    }

    /**
     * @return the breakTimes
     */
    public List<BreakTime> getBreakTimes() {
        return breakTimes;
    }

    /**
     * @param breakTimes the breakTimes to set
     */
    public void setBreakTimes(List<BreakTime> breakTimes) {
        this.breakTimes = breakTimes;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the arenaHasPreference
     */
    public boolean isArenaHasPreference() {
        return arenaHasPreference;
    }

    /**
     * @param arenaHasPreference the arenaHasPreference to set
     */
    public void setArenaHasPreference(boolean arenaHasPreference) {
        this.arenaHasPreference = arenaHasPreference;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the scheduleCompleted
     */
    public boolean isScheduleCompleted() {
        return scheduleCompleted;
    }

    /**
     * @param scheduleCompleted the scheduleCompleted to set
     */
    public void setScheduleCompleted(boolean scheduleCompleted) {
        this.scheduleCompleted = scheduleCompleted;
    }

}
