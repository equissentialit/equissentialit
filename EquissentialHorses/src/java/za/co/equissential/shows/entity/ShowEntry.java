/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import za.co.equissential.horse.entity.Horse;
import za.co.equissential.person.entity.Person;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "show_entry", schema = "equissential")
@NamedQueries( {
    @NamedQuery(name = "ShowEntry.findShowEntryForClassAfterTime", query = "SELECT e FROM ShowEntry e where e.showClass.id = :showClassId and e.approximateStartTime > :time and e.entrySwopped = false order by e.approximateStartTime"),
    @NamedQuery(name = "ShowEntry.findShowEntryForClassBeforeTime", query = "SELECT e FROM ShowEntry e where e.showClass.id = :showClassId and e.approximateStartTime < :time and e.entrySwopped = false order by e.approximateStartTime"),
    @NamedQuery(name = "ShowEntry.findShowEntryForClassRiderHorse", query = "SELECT e FROM ShowEntry e where e.showClass = :showClass and e.rider = :rider and e.horse = :horse")
})
public class ShowEntry implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "showEntryGen")
    @TableGenerator(name = "showEntryGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "SHOW_ENTRY_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "running_order")
    private int runningOrder;
    
    @Column(name = "rider_number")
    private String riderNumber;
    
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    @ManyToOne
    private Person rider;
    
   
//    @JoinColumn(name = "showclass_id", referencedColumnName = "id")
    @JoinTable(name="equissential.show_class_show_entry", 
          joinColumns=@JoinColumn(name="showEntries_id"),
          inverseJoinColumns=@JoinColumn(name="ShowClass_id"))
    @ManyToOne
    private ShowClass showClass;
    
    @JoinColumn(name = "horse_id", referencedColumnName = "id")
    @ManyToOne
    private Horse horse;
    
    @Column(name = "school_name")
    private String schoolName;
    
    @Column(name = "approx_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalTime approximateStartTime;  
    
    @Column(name = "entry_swopped")
    private boolean entrySwopped;

    /**
     * @return the runningOrder
     */
    public int getRunningOrder() {
        return runningOrder;
    }

    /**
     * @param runningOrder the runningOrder to set
     */
    public void setRunningOrder(int runningOrder) {
        this.runningOrder = runningOrder;
    }

    /**
     * @return the riderNumber
     */
    public String getRiderNumber() {
        return riderNumber;
    }

    /**
     * @param riderNumber the riderNumber to set
     */
    public void setRiderNumber(String riderNumber) {
        this.riderNumber = riderNumber;
    }

    /**
     * @return the approximateStartTime
     */
    public LocalTime getApproximateStartTime() {
        return approximateStartTime;
    }

    /**
     * @param approximateStartTime the approximateStartTime to set
     */
    public void setApproximateStartTime(LocalTime approximateStartTime) {
        this.approximateStartTime = approximateStartTime;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the rider
     */
    public Person getRider() {
        return rider;
    }

    /**
     * @param rider the rider to set
     */
    public void setRider(Person rider) {
        this.rider = rider;
    }

    /**
     * @return the horse
     */
    public Horse getHorse() {
        return horse;
    }

    /**
     * @param horse the horse to set
     */
    public void setHorse(Horse horse) {
        this.horse = horse;
    }

    /**
     * @return the schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * @param schoolName the schoolName to set
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

   
    /**
     * @return the showClass
     */
    public ShowClass getShowClass() {
        return showClass;
}

    /**
     * @param showClass the showClass to set
     */
    public void setShowClass(ShowClass showClass) {
        this.showClass = showClass;
    }

    /**
     * @return the entrySwopped
     */
    public boolean isEntrySwopped() {
        return entrySwopped;
    }

    /**
     * @param entrySwopped the entrySwopped to set
     */
    public void setEntrySwopped(boolean entrySwopped) {
        this.entrySwopped = entrySwopped;
    }
    
}
