/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "show_class", schema = "equissential")
        
public class ShowClass implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "showClassGen")
    @TableGenerator(name = "showClassGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "SHOW_CLASS_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "class_code")
    @NotNull(message = "Class Code cannot be null")
    private String code;
    
    @Column(name = "class_name")
    @NotNull(message = "Class name cannot be null")
    private String className;
    
    @Column(name = "test_link")
    private String testLink;
    
    @Column(name = "test_details")
    private String testDetails;
    
    @Column(name = "test_name")
    private String testName;
    
    @Column(name = "class_level")
    private int classLevel;
    
    @Column(name = "approx_start_time")
    @Temporal(TemporalType.TIME)
    private LocalTime approximateStartTime;
    
    @Column(name = "arena_order")
    private int arenaOrder;
    
    @OneToMany(mappedBy = "showClass", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="equissential.show_class_show_entry", 
          joinColumns=@JoinColumn(name="ShowClass_id"),
          inverseJoinColumns=@JoinColumn(name="showEntries_id"))
    private List<ShowEntry>  showEntries = new ArrayList<>();
    
    @OneToOne
    @JoinColumn(name = "class_type_id", referencedColumnName = "id")
    private ClassType classType;
    
    @OneToOne
    @JoinColumn(name = "follow_on_class", referencedColumnName = "id")
    private ShowClass followOnClass;
    
    @Column(name = "is_follow_on")
    private boolean isFollowOn;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the testLink
     */
    public String getTestLink() {
        return testLink;
    }

    /**
     * @param testLink the testLink to set
     */
    public void setTestLink(String testLink) {
        this.testLink = testLink;
    }

    /**
     * @return the testDetails
     */
    public String getTestDetails() {
        return testDetails;
    }

    /**
     * @param testDetails the testDetails to set
     */
    public void setTestDetails(String testDetails) {
        this.testDetails = testDetails;
    }

    /**
     * @return the approximateStartTime
     */
    public LocalTime getApproximateStartTime() {
        return approximateStartTime;
    }

    /**
     * @param approximateStartTime the approximateStartTime to set
     */
    public void setApproximateStartTime(LocalTime approximateStartTime) {
        this.approximateStartTime = approximateStartTime;
    }

    /**
     * @return the arenaOrder
     */
    public int getArenaOrder() {
        return arenaOrder;
    }

    /**
     * @param arenaOrder the arenaOrder to set
     */
    public void setArenaOrder(int arenaOrder) {
        this.arenaOrder = arenaOrder;
    }

    /**
     * @return the competitors
     */
    public List<ShowEntry> getShowEntries() {
        return showEntries;
    }

    /**
     * @param showEntries the competitors to set
     */
    public void setShowEntries(List<ShowEntry> showEntries) {
        this.showEntries = showEntries;
    }

    /**
     * @return the classType
     */
    public ClassType getClassType() {
        return classType;
    }

    /**
     * @param classType the classType to set
     */
    public void setClassType(ClassType classType) {
        this.classType = classType;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * @param testName the testName to set
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }

    /**
     * @return the classLevel
     */
    public int getClassLevel() {
        return classLevel;
    }

    /**
     * @param classLevel the classLevel to set
     */
    public void setClassLevel(int classLevel) {
        this.classLevel = classLevel;
    }

    public void addShowEntry(ShowEntry competitor) {
        getShowEntries().add(competitor);
    }

    /**
     * @return the followOnClass
     */
    public ShowClass getFollowOnClass() {
        return followOnClass;
    }

    /**
     * @param followOnClass the followOnClass to set
     */
    public void setFollowOnClass(ShowClass followOnClass) {
        this.followOnClass = followOnClass;
    }

    /**
     * @return the isFollowOnClass
     */
    public boolean isIsFollowOn() {
        return isFollowOn;
    }

    /**
     * @param isFollowOn the isFollowOn to set
     */
    public void setIsFollowOn(boolean isFollowOn) {
        this.isFollowOn = isFollowOn;
    }
    
}
