/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.organisation.Organisation;
import za.co.equissential.person.entity.Person;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "show", schema = "equissential")
@NamedQueries( {
    @NamedQuery(name = "Show.findByNameAndDate", query = "SELECT s FROM Show s where s.showName = :showName and s.startDate <= :date and s.endDate >= :date"),
    @NamedQuery(name = "Show.findShowsByOrganisationId", query = "SELECT s FROM Show s where s.organisation.id = :organisationId order by s.startDate"),
    @NamedQuery(name = "Show.findShowProgramsForDate", query = "SELECT sp From Show s, in(s.showPrograms) sp where s.id = :showId and sp.date = :date")
})
public class Show implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "showGen")
    @TableGenerator(name = "showGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "SHOW_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "show_name")
    @NotNull(message = "Show name cannot be null")
    private String showName;
    
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private LocalDate startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private LocalDate endDate;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "show_id", referencedColumnName = "id")
    private List<ShowProgram> showPrograms = new ArrayList<>();
    
    @OneToOne
    @JoinColumn(name = "show_venue_id", referencedColumnName = "id")
    private ShowVenue venue;
    
    @OneToOne
    @JoinColumn(name = "organisation_id", referencedColumnName = "id")
    private Organisation organisation;
    
    @Column(name = "competitor_organisation_member")
    private boolean competitorMustBeOrganisationMember;

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the venue
     */
    public ShowVenue getVenue() {
        return venue;
    }

    /**
     * @param venue the venue to set
     */
    public void setVenue(ShowVenue venue) {
        this.venue = venue;
    }

    /**
     * @return the organisation
     */
    public Organisation getOrganisation() {
        return organisation;
    }

    /**
     * @param organisation the organisation to set
     */
    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    /**
     * @return the competitorMustBeOrganisationMember
     */
    public boolean isCompetitorMustBeOrganisationMember() {
        return competitorMustBeOrganisationMember;
    }

    /**
     * @param competitorMustBeOrganisationMember the competitorMustBeOrganisationMember to set
     */
    public void setCompetitorMustBeOrganisationMember(boolean competitorMustBeOrganisationMember) {
        this.competitorMustBeOrganisationMember = competitorMustBeOrganisationMember;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the showName
     */
    public String getShowName() {
        return showName;
    }

    /**
     * @param showName the showName to set
     */
    public void setShowName(String showName) {
        this.showName = showName;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the showProgram
     */
    public List<ShowProgram> getShowPrograms() {
        return showPrograms;
    }

    /**
     * @param showProgram the showProgram to set
     */
    public void setShowPrograms(List<ShowProgram> showProgram) {
        this.showPrograms = showProgram;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(showName);
        builder.append(" (").append(getStartDate().toString()).append(" - ").append(getEndDate().toString()).append(")");
        return builder.toString();
    }

    public ArrayList<LocalDate> getAllShowDates() {
        ArrayList<LocalDate> showDates = new ArrayList();
        LocalDate newDate = startDate;
        while (newDate.isBefore(endDate) || newDate.equals(endDate)) {
            showDates.add(newDate);
            newDate = newDate.plusDays(1);
        }
        return showDates;
    }

    
}
