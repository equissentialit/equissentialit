/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "class_type", schema = "equissential")
public class ClassType implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "classTypeGen")
    @TableGenerator(name = "classTypeGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "CLASS_TYPE_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @NotNull(message = "Class Type name cannot be null")
    @Column(name = "class_type_name")
    private String name;
    
    @Column(name = "sub_type")
    private String subType;
    
    @Column(name = "course_walk_time_minutes")
    private long courseWalkInMinutes;
    
    @Column(name = "time_per_competitor_minutes")
    private long timePerCompetitor;
    
    @Column(name = "time_for_prizegiving")
    private long timeForPrizegiving;
    
    @Column(name = "display_rider_start_time")
    private boolean displayRiderStartTime;
    
    @Column(name = "class_type_has_preference")
    private boolean classTypeHasPreference;
    
    

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the courseWalkInMinutes
     */
    public long getCourseWalkInMinutes() {
        return courseWalkInMinutes;
    }

    /**
     * @param courseWalkInMinutes the courseWalkInMinutes to set
     */
    public void setCourseWalkInMinutes(long courseWalkInMinutes) {
        this.courseWalkInMinutes = courseWalkInMinutes;
    }

    /**
     * @return the timePerCompetitor
     */
    public long getTimePerCompetitor() {
        return timePerCompetitor;
    }

    /**
     * @param timePerCompetitor the timePerCompetitor to set
     */
    public void setTimePerCompetitor(long timePerCompetitor) {
        this.timePerCompetitor = timePerCompetitor;
    }

    /**
     * @return the displayRiderStartTime
     */
    public boolean isDisplayRiderStartTime() {
        return displayRiderStartTime;
    }

    /**
     * @param displayRiderStartTime the displayRiderStartTime to set
     */
    public void setDisplayRiderStartTime(boolean displayRiderStartTime) {
        this.displayRiderStartTime = displayRiderStartTime;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    
    /**
     * @return the classTypeHasPreference
     */
    public boolean isClassTypeHasPreference() {
        return classTypeHasPreference;
    }

    /**
     * @param classTypeHasPreference the classTypeHasPreference to set
     */
    public void setClassTypeHasPreference(boolean classTypeHasPreference) {
        this.classTypeHasPreference = classTypeHasPreference;
    }

    /**
     * @return the timeForPrizegiving
     */
    public long getTimeForPrizegiving() {
        return timeForPrizegiving;
    }

    /**
     * @param timeForPrizegiving the timeForPrizegiving to set
     */
    public void setTimeForPrizegiving(long timeForPrizegiving) {
        this.timeForPrizegiving = timeForPrizegiving;
    }
}
