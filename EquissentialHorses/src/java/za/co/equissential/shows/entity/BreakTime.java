/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "break_time", schema = "equissential")
public class BreakTime implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "breakTimeGen")
    @TableGenerator(name = "breakTimeGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "BREAK_TIME_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "break_time")
    @Temporal(TemporalType.TIME)
    private LocalTime breakTime;
     
    @Column(name = "break_duration_minutes")
    @NotNull(message = "Break in minutes cannot be null")
    private int breakDurationInMinutes;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BreakTime)) {
            return false;
        }
        BreakTime other = (BreakTime) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.co.equissential.shows.entity.BreakTime[ id=" + id + " ]";
    }

    /**
     * @return the breakTime
     */
    public LocalTime getBreakTime() {
        return breakTime;
    }

    /**
     * @param breakTime the breakTime to set
     */
    public void setBreakTime(LocalTime breakTime) {
        this.breakTime = breakTime;
    }

    /**
     * @return the breakDurationInMinutes
     */
    public int getBreakDurationInMinutes() {
        return breakDurationInMinutes;
    }

    /**
     * @param breakDurationInMinutes the breakDurationInMinutes to set
     */
    public void setBreakDurationInMinutes(int breakDurationInMinutes) {
        this.breakDurationInMinutes = breakDurationInMinutes;
    }
    
}
