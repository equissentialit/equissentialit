/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "show_venue", schema = "equissential")
public class ShowVenue implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "showVenueGen")
    @TableGenerator(name = "showVenueGen", 
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "SHOW_VENUE_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "venue_name")
    @NotNull(message = "Venue name cannot be null")
    private String venueName;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "show_venue_id", referencedColumnName = "id")
    private List<Arena> arenas = new ArrayList<>();

    /**
     * @return the name
     */
    public String getName() {
        return venueName;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.venueName = name;
    }

    /**
     * @return the arenas
     */
    public List<Arena> getArenas() {
        return arenas;
    }

    /**
     * @param arenas the arenas to set
     */
    public void setArenas(List<Arena> arenas) {
        this.arenas = arenas;
    }
    
    
}
