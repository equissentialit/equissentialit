/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.util.Comparator;
import za.co.equissential.shows.entity.ShowProgram;

/**
 *
 * @author liezelbedggood
 */
public class ShowProgramComparator implements Comparator<ShowProgram> {

    @Override
    public int compare(ShowProgram program1, ShowProgram program2) {
        int compare;
        if (program1.getDate().equals(program2.getDate())) {
            if (program1.isArenaHasPreference() == program2.isArenaHasPreference()) {
                if (program1.getStartTime().isBefore(program2.getStartTime())) {
                    compare = -1;
                } else {
                    if (program1.getStartTime().isAfter(program2.getStartTime())) {
                        compare = 1;
                    } else {
                        compare = 0;
                    }
                }
            } else {
                if (program1.isArenaHasPreference()) {
                    compare = -1;
                } else {
                    compare = 1;
                }
            }
        } else {
            if (program1.getDate().isBefore(program2.getDate())) {
                compare = -1;
            } else {
                compare = 1;
            }
        }
        return compare;
    }

}
