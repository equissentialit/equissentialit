/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.util.Comparator;
import za.co.equissential.shows.entity.ShowEntry;

/**
 *
 * @author liezelbedggood
 */
public class ShowEntryTimeComparator  implements Comparator<ShowEntry>{
    
    @Override
    public int compare(ShowEntry entry1, ShowEntry entry2) {
        int compare;
        if (entry1.getApproximateStartTime().equals(entry2.getApproximateStartTime())) {
            compare = 0;
        } else {
            if (entry1.getApproximateStartTime().isBefore(entry2.getApproximateStartTime())) {
                compare = -1;
            } else {
                compare = 1;
            }
        }
        return compare;
    }
    
}
