/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author liezelbedggood
 */
public class ShowScheduleTO {
    private String showName;
    private LocalDate startDate;
    private LocalDate endDate;
    private String venueName;
    
    private ArrayList<ShowProgramTO> showProgramTOs;

    /**
     * @return the showName
     */
    public String getShowName() {
        return showName;
    }

    /**
     * @param showName the showName to set
     */
    public void setShowName(String showName) {
        this.showName = showName;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the venueName
     */
    public String getVenueName() {
        return venueName;
    }

    /**
     * @param venueName the venueName to set
     */
    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    /**
     * @return the showProgramTOs
     */
    public ArrayList<ShowProgramTO> getShowProgramTOs() {
        return showProgramTOs;
    }

    /**
     * @param showProgramTOs the showProgramTOs to set
     */
    public void setShowProgramTOs(ArrayList<ShowProgramTO> showProgramTOs) {
        this.showProgramTOs = showProgramTOs;
    }
    
    
}
