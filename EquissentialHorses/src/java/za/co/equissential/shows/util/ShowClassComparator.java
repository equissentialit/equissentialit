/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.util.Comparator;
import za.co.equissential.shows.entity.ShowClass;

/**
 *
 * @author liezelbedggood
 */
public class ShowClassComparator  implements Comparator<ShowClass>{
    
    @Override
    public int compare(ShowClass class1, ShowClass class2) {
        int compare;
        if (class1.getArenaOrder() == class2.getArenaOrder()) {
            compare = 0;
        } else {
            if (class1.getArenaOrder() < class2.getArenaOrder()) {
                compare = -1;
            } else {
                compare = 1;
            }
        }
        return compare;
    }
    
}
