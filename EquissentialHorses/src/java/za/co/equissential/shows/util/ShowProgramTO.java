/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author liezelbedggood
 */
public class ShowProgramTO {
    private String arenaName;
    private LocalDate date;
    private LocalTime startTime;
    
    private ArrayList<ShowClassTO> showClassTOs;

    /**
     * @return the arenaName
     */
    public String getArenaName() {
        return arenaName;
    }

    /**
     * @param arenaName the arenaName to set
     */
    public void setArenaName(String arenaName) {
        this.arenaName = arenaName;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the startTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the showClassTOs
     */
    public ArrayList<ShowClassTO> getShowClassTOs() {
        return showClassTOs;
    }

    /**
     * @param showClassTOs the showClassTOs to set
     */
    public void setShowClassTOs(ArrayList<ShowClassTO> showClassTOs) {
        this.showClassTOs = showClassTOs;
    }
}
