/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author liezelbedggood
 */
public class ShowClassTO {
    private String code;
    private String name;
    private String testLink;
    private String testDetails;
    private String testName;
    private int arenaOrder;
    private int classLevel;
    private LocalTime approximateStartTime;
    
    private ArrayList<ShowEntryTO> showEntryTOs;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the testLink
     */
    public String getTestLink() {
        return testLink;
    }

    /**
     * @param testLink the testLink to set
     */
    public void setTestLink(String testLink) {
        this.testLink = testLink;
    }

    /**
     * @return the testDetails
     */
    public String getTestDetails() {
        return testDetails;
    }

    /**
     * @param testDetails the testDetails to set
     */
    public void setTestDetails(String testDetails) {
        this.testDetails = testDetails;
    }

    /**
     * @return the testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * @param testName the testName to set
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }

    /**
     * @return the arenaOrder
     */
    public int getArenaOrder() {
        return arenaOrder;
    }

    /**
     * @param arenaOrder the arenaOrder to set
     */
    public void setArenaOrder(int arenaOrder) {
        this.arenaOrder = arenaOrder;
    }

    /**
     * @return the classLevel
     */
    public int getClassLevel() {
        return classLevel;
    }

    /**
     * @param classLevel the classLevel to set
     */
    public void setClassLevel(int classLevel) {
        this.classLevel = classLevel;
    }

    /**
     * @return the approximateStartTime
     */
    public LocalTime getApproximateStartTime() {
        return approximateStartTime;
    }

    /**
     * @param approximateStartTime the approximateStartTime to set
     */
    public void setApproximateStartTime(LocalTime approximateStartTime) {
        this.approximateStartTime = approximateStartTime;
    }

    /**
     * @return the showEntryTOs
     */
    public ArrayList<ShowEntryTO> getShowEntryTOs() {
        return showEntryTOs;
    }

    /**
     * @param showEntryTOs the showEntryTOs to set
     */
    public void setShowEntryTOs(ArrayList<ShowEntryTO> showEntryTOs) {
        this.showEntryTOs = showEntryTOs;
    }
}
