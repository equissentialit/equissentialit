/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.shows.util;

import java.time.LocalTime;

/**
 *
 * @author liezelbedggood
 */
public class ShowEntryTO {
    private int runningOrder;
    private String riderNumber;
    private String riderName;
    private String riderSurname;
    private String horseName;
    private String schoolName;
    private LocalTime approximateStartTime;

    /**
     * @return the runningOrder
     */
    public int getRunningOrder() {
        return runningOrder;
    }

    /**
     * @param runningOrder the runningOrder to set
     */
    public void setRunningOrder(int runningOrder) {
        this.runningOrder = runningOrder;
    }

    /**
     * @return the riderNumber
     */
    public String getRiderNumber() {
        return riderNumber;
    }

    /**
     * @param riderNumber the riderNumber to set
     */
    public void setRiderNumber(String riderNumber) {
        this.riderNumber = riderNumber;
    }

    /**
     * @return the riderName
     */
    public String getRiderName() {
        return riderName;
    }

    /**
     * @param riderName the riderName to set
     */
    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    /**
     * @return the riderSurname
     */
    public String getRiderSurname() {
        return riderSurname;
    }

    /**
     * @param riderSurname the riderSurname to set
     */
    public void setRiderSurname(String riderSurname) {
        this.riderSurname = riderSurname;
    }

    /**
     * @return the horseName
     */
    public String getHorseName() {
        return horseName;
    }

    /**
     * @param horseName the horseName to set
     */
    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    /**
     * @return the schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * @param schoolName the schoolName to set
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     * @return the approximateStartTime
     */
    public LocalTime getApproximateStartTime() {
        return approximateStartTime;
    }

    /**
     * @param approximateStartTime the approximateStartTime to set
     */
    public void setApproximateStartTime(LocalTime approximateStartTime) {
        this.approximateStartTime = approximateStartTime;
    }
    
}
