/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.util;

import java.util.ArrayList;
import za.co.equissential.shows.entity.Show;
import za.co.equissential.shows.entity.ShowClass;
import za.co.equissential.shows.entity.ShowEntry;
import za.co.equissential.shows.entity.ShowProgram;
import za.co.equissential.shows.util.ShowClassTO;
import za.co.equissential.shows.util.ShowEntryTO;
import za.co.equissential.shows.util.ShowScheduleTO;
import za.co.equissential.shows.util.ShowProgramTO;

/**
 *
 * @author liezelbedggood
 */
public class TOUtil {

    public static ShowScheduleTO convertShowToShowScheduleTO(Show show) {
        ShowScheduleTO scheduleTO = new ShowScheduleTO();
        scheduleTO.setShowName(show.getShowName());
        scheduleTO.setVenueName(show.getVenue().getName());
        scheduleTO.setStartDate(show.getStartDate());
        scheduleTO.setEndDate(show.getEndDate());
        ArrayList<ShowProgramTO> showPrograms = new ArrayList<>();
        show.getShowPrograms().forEach(showProgram -> {
            showPrograms.add(createShowProgramTOFromShowProgram(showProgram));
        });
        scheduleTO.setShowProgramTOs(showPrograms);
        return scheduleTO;
    }

    public static ShowProgramTO createShowProgramTOFromShowProgram(ShowProgram showProgram) {
        ShowProgramTO showProgramTO = new ShowProgramTO();
        showProgramTO.setArenaName(showProgram.getArena().getArenaName());
        showProgramTO.setDate(showProgram.getDate());
        showProgramTO.setStartTime(showProgram.getStartTime());
        ArrayList<ShowClassTO> showClasses = new ArrayList<>();
        showProgram.getClassesForArena().forEach(showClass -> {
            showClasses.add(createShowClassTOFromShowClass(showClass));
        });
        showProgramTO.setShowClassTOs(showClasses);
        return showProgramTO;
    }

    public static ShowClassTO createShowClassTOFromShowClass(ShowClass showClass) {
        ShowClassTO showClassTO = new ShowClassTO();
        showClassTO.setName(showClass.getClassName());
        showClassTO.setCode(showClass.getCode());
        showClassTO.setClassLevel(showClass.getClassLevel());
        showClassTO.setTestName(showClass.getTestName());
        showClassTO.setTestDetails(showClass.getTestDetails());
        showClassTO.setTestLink(showClass.getTestLink());
        showClassTO.setArenaOrder(showClass.getArenaOrder());
        showClassTO.setApproximateStartTime(showClass.getApproximateStartTime());
        ArrayList<ShowEntryTO> showEntries = new ArrayList<>();
        showClass.getShowEntries().forEach(showEntry -> {
            showEntries.add(createShowEntryTOFromShowEntry(showEntry));
        });
        showClassTO.setShowEntryTOs(showEntries);
        return showClassTO;
    }
    
    public static ShowEntryTO createShowEntryTOFromShowEntry(ShowEntry showEntry) {
        ShowEntryTO showEntryTO = new ShowEntryTO();
        showEntryTO.setRunningOrder(showEntry.getRunningOrder());
        showEntryTO.setApproximateStartTime(showEntry.getApproximateStartTime());
        showEntryTO.setRiderName(showEntry.getRider().getFirstName());
        showEntryTO.setRiderSurname(showEntry.getRider().getSurname());
        showEntryTO.setRiderNumber(showEntry.getRiderNumber());
        showEntryTO.setHorseName(showEntry.getHorse().getName());
        showEntryTO.setSchoolName(showEntry.getSchoolName());
        return showEntryTO;
    }
}
