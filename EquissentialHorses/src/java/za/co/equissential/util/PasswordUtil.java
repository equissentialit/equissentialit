package za.co.equissential.util;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import za.co.equissential.exceptions.SecurityException;

/**
 * Generate cryptographically strong unique passwords.
 */
public class PasswordUtil {

    private static final int ITERATIONS = 120000;

    /**
     * Returns a salted and hashed password.<br>
     *
     * @param password The password to be hashed.
     * @return The iterations, salt and hashed password.
     * @throws SecurityException when the algorithm or key spec is invalid.
     */
    public static String generateStrongPasswordHash(String password) throws SecurityException {
        try {
            byte[] salt = getSalt();

            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATIONS, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return ITERATIONS + ":" + toHex(salt) + ":" + toHex(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new SecurityException(ex);
        }
    }

    /**
     * Returns a random salt to be used to hash a password.
     *
     * @return A 16 bytes random salt.
     * @throws NoSuchAlgorithmException if the provided algorithm is invalid.
     */
    public static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    /**
     * Convert a byte[] to hex.
     *
     * @param array The byte[] to convert.
     * @return The byte[] as hex.
     */
    public static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    /**
     * Validates that the supplied password, when hashed, matches the hash
     * password stored.
     *
     * @param originalPassword The password to verify.
     * @param storedPassword The password to verify against.
     * @return True if the passwords match, otherwise False.
     * @throws SecurityException when the algorithm or key spec is invalid.
     */
    public static boolean validatePassword(String originalPassword, String storedPassword) throws SecurityException {
        try {
            String[] parts = storedPassword.split(":");
            int iterations = Integer.parseInt(parts[0]);
            byte[] salt = fromHex(parts[1]);
            byte[] hash = fromHex(parts[2]);

            PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] testHash = skf.generateSecret(spec).getEncoded();

            int diff = hash.length ^ testHash.length;
            for (int i = 0; i < hash.length && i < testHash.length; i++) {
                diff |= hash[i] ^ testHash[i];
            }
            return diff == 0;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new SecurityException(ex);
        }
    }

    /**
     * Hash the supplied password, based on the password stored.
     *
     * @param originalPassword The password to hash.
     * @param storedPassword The stored password.
     * @return The hashed password.
     * @throws SecurityException when the algorithm or key spec is invalid.
     */
    public static String hashPassword(String originalPassword, String storedPassword) throws SecurityException {
        try {
            String[] parts = storedPassword.split(":");
            int iterations = Integer.parseInt(parts[0]);
            byte[] salt = fromHex(parts[1]);
            byte[] hash = fromHex(parts[2]);

            PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] testHash = skf.generateSecret(spec).getEncoded();

            return ITERATIONS + ":" + toHex(salt) + ":" + toHex(testHash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new SecurityException(ex);
        }
    }
    
    /**
     * Convert from hex to byte[].
     *
     * @param hex The hex value to convert.
     * @return A byte[].
     */
    public static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
