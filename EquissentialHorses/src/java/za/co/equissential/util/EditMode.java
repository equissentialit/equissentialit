package za.co.equissential.util;

/**
 * Defines the states of a registration request.
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>Add  - add a new entity.</li>
 *      <li>Edit - modify an existing entity</li>
 *  </ul> 
 */
public enum EditMode {

    ADD("Add"),
    EDIT("Edit");
    private final String editMode;

    EditMode(String editMode) {
        this.editMode = editMode;
    }

    public String value() {
        return editMode;
    }  
}
