package za.co.equissential.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import za.co.equissential.person.entity.Person;

/**
 * Group commonly used session calls.
 */
public class SessionUtil {

    public static Person getLoggedInUser() {
        return (Person) getSession().getAttribute("person");        
    }
    
    public static HttpSession getSession() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (HttpSession) context.getExternalContext().getSession(false);
    }
}
