package za.co.equissential.util;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import za.co.equissential.person.entity.Person;
import za.co.equissential.organisation.OrganisationMembership;
import za.co.equissential.organisation.OrganisationRole;
import za.co.equissential.util.SessionUtil;

@Named
@SessionScoped
public class SystemBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean adminAllowed = false;

    private boolean systemAdminAllowed = false;
    
    private SystemMode systemMode;

    private String theme = "blue";

    private List<OrganisationMembership> organisationsToAdmin;

    private OrganisationMembership selectedOrganisationPerson;

    public SystemBean() {
    }

    public void init() {
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("EMMAAdmin")) {
            adminAllowed = true;
        }
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("EMMASystemAdmin")) {
            systemAdminAllowed = true;
        }
        setUserMode();
        getOrganisationsToAdmin(SessionUtil.getLoggedInUser());
        if (organisationsToAdmin.size() == 1) {
            setSelectedOrganisationPerson(organisationsToAdmin.get(0));
        }
    }

    public String setUserMode() {
        systemMode = SystemMode.USER;
        theme = "blue";
        setSelectedOrganisationPerson(null);
        return "/app/dashboard?faces-redirect=true";
    }

    public String setAdminMode() {
        systemMode = SystemMode.ADMIN;
        theme = "red";
        if (organisationsToAdmin .size() > 0) {
            setSelectedOrganisationPerson(organisationsToAdmin.get(0));
        }
        return "/app/dashboard?faces-redirect=true";
    }

    public String setSysAdminMode() {
        systemMode = SystemMode.SYSADMIN;
        theme = "red";
        setSelectedOrganisationPerson(null);
        return "/app/dashboard?faces-redirect=true";
    }

    public boolean isUserMode() {
        return systemMode.equals(SystemMode.USER);
    }

    public boolean isAdminMode() {
        return systemMode.equals(SystemMode.ADMIN);
    }

    public boolean isSystemAdminMode() {
        return systemMode.equals(SystemMode.SYSADMIN);
    }
    
    public boolean canSwitchToAdmin() {
        return adminAllowed && !isAdminMode();
    }
    
    public boolean canSwitchToSystemAdmin() {
        return systemAdminAllowed && !isSystemAdminMode();
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSystemMode() {
        return systemMode.value();
    }

    public boolean isSystemAdminAllowed() {
        return systemAdminAllowed;
    }

    public void setSystemAdminAllowed(boolean systemAdminAllowed) {
        this.systemAdminAllowed = systemAdminAllowed;
    }

    public boolean isAdminAllowed() {
        return adminAllowed;
    }

    public void setAdminAllowed(boolean adminAllowed) {
        this.adminAllowed = adminAllowed;
    }

    public List<OrganisationMembership> getOrganisationsToAdmin() {
        return organisationsToAdmin;
    }

    public void setOrganisationsToAdmin(List<OrganisationMembership> organisationsToAdmin) {
        this.organisationsToAdmin = organisationsToAdmin;
    }

    public OrganisationMembership getSelectedOrganisationPerson() {
        return selectedOrganisationPerson;
    }

    public void setSelectedOrganisationPerson(OrganisationMembership  selectedOrganisationPerson) {
        this.selectedOrganisationPerson = selectedOrganisationPerson;
    }

    /**
     * Get a list of clubs the person is an administrator of.
     *
     * @param person the person to get the list of organisations for
     * @return a list of organisationPersonTO that the person can administer.
     */
    private List<OrganisationMembership> getOrganisationsToAdmin(Person person) {
        organisationsToAdmin = person.getMemberships().stream()
                .filter(organisationPerson -> organisationPerson.getOrganisationRoles().equals(OrganisationRole.ADMINISTRATOR))
                .collect(Collectors.toList());
        return organisationsToAdmin;
    }

    public String switchOrganisation() {
        return "/app/dashboard?faces-redirect=true";
    }
}
