package za.co.equissential.util;

/**
 * Defines the modes of the system.
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>User - normal user mode.</li>
 *      <li>Admin - organisation administrator mode.</li>
 *      <li>SysAdmin - system administrator mode.</li>
 *  </ul> 
 */
public enum SystemMode {

    USER("User"),
    ADMIN("Admin"),
    SYSADMIN("SysAdmin");

    private final String systemMode;

    SystemMode(String systemMode) {
        this.systemMode = systemMode;
    }

    public String value() {
        return systemMode;
    }  
}
