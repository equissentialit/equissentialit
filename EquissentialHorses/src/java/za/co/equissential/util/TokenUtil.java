package za.co.equissential.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import static za.co.equissential.util.PasswordUtil.fromHex;
import static za.co.equissential.util.PasswordUtil.toHex;

/**
 * Generate cryptographically strong unique tokens.
 */
public class TokenUtil {

    private static final int ITERATIONS = 120000;

    /**
     * Generate a cryptographically strong random number.
     *
     * @return A string representation of the token in a radix of 16.
     */
    public static String generateToken() {
        SecureRandom random = new SecureRandom();
        long longToken = random.nextLong();
        String token = Long.toString(longToken, 16);
        return token;
    }

    /**
     * Returns a salted and hashed token using the provided hash.<br>
     *
     * @param token the token to be hashed.
     * @return The iterations, salt and hashed password.
     * @throws SecurityException when the algorithm or key spec is invalid.
     */
    public static String generateStrongTokenHash(String token) throws SecurityException {
        try {
            char[] chars = token.toCharArray();
            byte[] salt = getSalt();

            PBEKeySpec spec = new PBEKeySpec(chars, salt, ITERATIONS, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return ITERATIONS + ":" + toHex(salt) + ":" + toHex(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new SecurityException(ex);
        }
    }

    /**
     * Returns a random salt to be used to hash a password.
     *
     * @return A 16 bytes random salt.
     * @throws NoSuchAlgorithmException if the specified algorithm is invalid.
     */
    public static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    /**
     * Validates that the supplied password, when hashed, matches the hash
     * password stored.
     *
     * @param originalToken the token to verify.
     * @param storedToken The token to verify against.
     * @return True if the tokens match, otherwise False.
     * @throws SecurityException when the algorithm or key spec is invalid.
     */
    public static boolean validateToken(String originalToken, String storedToken) throws SecurityException {
        try {
            String[] parts = storedToken.split(":");
            int iterations = Integer.parseInt(parts[0]);
            byte[] salt = fromHex(parts[1]);
            byte[] hash = fromHex(parts[2]);

            PBEKeySpec spec = new PBEKeySpec(originalToken.toCharArray(), salt, iterations, hash.length * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] testHash = skf.generateSecret(spec).getEncoded();

            int diff = hash.length ^ testHash.length;
            for (int i = 0; i < hash.length && i < testHash.length; i++) {
                diff |= hash[i] ^ testHash[i];
            }
            return diff == 0;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new SecurityException(ex);
        }
    }
}