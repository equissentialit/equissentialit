package za.co.equissential.util;

import java.util.ResourceBundle;

public abstract class BaseBean {

    private ResourceBundle msgBundle;

    public ResourceBundle getBundle() {
        if (msgBundle == null) {
            msgBundle = ResourceBundle.getBundle("messages");
        }
        return msgBundle;
    }   
}
