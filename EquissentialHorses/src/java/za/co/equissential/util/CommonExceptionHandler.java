package za.co.equissential.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.NonexistentConversationException;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

/**
 * A common exception handler that will handle all JSF exceptions.
 */
public class CommonExceptionHandler extends ExceptionHandlerWrapper {

    private final ExceptionHandler wrapped;
    private static final Logger LOGGER = Logger.getLogger(CommonExceptionHandler.class.getName());

    public CommonExceptionHandler(final javax.faces.context.ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public javax.faces.context.ExceptionHandler getWrapped() {
        return this.wrapped;
    }

    @Override
    public void handle() throws FacesException {
        for (final Iterator<ExceptionQueuedEvent> it = getUnhandledExceptionQueuedEvents().iterator(); it.hasNext();) {
            ExceptionQueuedEvent event = it.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
            Throwable throwable = context.getException();
            try {
                if (throwable instanceof ViewExpiredException) {
                    handleException(throwable);
                } else {
                    Throwable cause = getNonexistentConversationException(throwable);
                    if (cause != null) {
                        handleException(cause);
                    }
                }
            } finally {
                it.remove();
            }
        }
        getWrapped().handle();
    }

    private Throwable getNonexistentConversationException(Throwable throwable) {   
        Throwable cause = throwable.getCause();
        while (!(cause instanceof NonexistentConversationException)  && cause != null) {
            cause = cause.getCause();
        }
        if (cause instanceof NonexistentConversationException) {
            return cause;
        }
        else {
            return null;
        }
    }

    private void handleException(Throwable throwable) {
        final FacesContext facesContext = FacesContext.getCurrentInstance();
        final ExternalContext externalContext = facesContext.getExternalContext();

        try {
            if (throwable instanceof ViewExpiredException) {
                externalContext.redirect("/EMMAWeb/faces/app/index.xhtml?faces-redirect=true");
                facesContext.addMessage(null, new FacesMessage("View expired"));
            } else {
                externalContext.redirect("/EMMAWeb/faces/app/index.xhtml?faces-redirect=true&nocid=true");
                facesContext.addMessage(null, new FacesMessage("View expired"));
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error view 'index.xhtml' unknown!", e);
        }
        facesContext.responseComplete();
    }
}
