package za.co.equissential.notification;

/**
 * Defines the states of a registration request.
 * 
 * The statuses are as follows:
 *  <ul>
 *      <li>Pending  - the notification is waiting to be sent</li>
 *      <li>Sent - the notification has been sent.</li>
 *  </ul> 
 */
public enum NotificationStatus {

    PENDING("Pending"),
    SENT("Sent");

    private final String notificationStatus;

    NotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String value() {
        return notificationStatus;
    }  
}
