package za.co.equissential.notification;



import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.auth.web.RegistrationRequest;
import za.co.equissential.person.entity.Person;

/**
 * Session Bean implementation class NotificationController
 */
@Stateless
@LocalBean
@Interceptors(AuditInterceptor.class)
public class NotificationController {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource(name = "mail/EquissentialTest")
    private Session mailSession;

    private final Configuration cfg;

    public NotificationController() {
        cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(NotificationController.class, "");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
    }

    /**
     * Cache the password change request in the EMail Spool cache.
     *
     * @param token The token for this reset password request.
     * @param person The person to send the email to.
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void submitResetPasswordRequest(String token, Person person) {
        try {
            String appPath = System.getProperty("EQUISSENTIAL_APP_PATH", "");
            StringBuilder resetPathBuilder = new StringBuilder(appPath);
            resetPathBuilder.append("/resetPassword.xhtml?ml=");
            resetPathBuilder.append(person.getEmail());
            resetPathBuilder.append("&tk=");
            resetPathBuilder.append(token);

            Map root = new HashMap();
            root.put("name", person.getFirstName());
            root.put("link", resetPathBuilder);
            Template template = cfg.getTemplate("passwordReset.ftl");
            StringWriter writer = new StringWriter();
            template.process(root, writer);
            String mailContent = writer.toString();

            String subject = person.getFirstName()+ ", here's the link to reset your Equissential password.";
            writeToCache(person.getEmail(), subject, mailContent, false, true, null, null);
        } catch (TemplateException | MessagingException | IOException ex) {
            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Cache the register request in the EMail Spool cache.
     *
     * @param token The token for this reset password request.
     * @param registrationRequest The details of the registration request.
     */
    @PermitAll
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void submitRegisterRequest(String token, RegistrationRequest registrationRequest) {
        try {
            String appPath = System.getProperty("EQUISSENTIAL_APP_PATH", "");
            StringBuilder resetPathBuilder = new StringBuilder(appPath);
            resetPathBuilder.append("/registerVerification.xhtml?ml=");
            resetPathBuilder.append(registrationRequest.getEmail());
            resetPathBuilder.append("&tk=");
            resetPathBuilder.append(token);

            Map root = new HashMap();
            root.put("name", registrationRequest.getName());
            root.put("link", resetPathBuilder);
            Template template = cfg.getTemplate("registerConfirmation.ftl");
            StringWriter writer = new StringWriter();
            template.process(root, writer);
            String mailContent = writer.toString();

            String subject = registrationRequest.getName() + ", here's the link to confirm your sign up.";
            writeToCache(registrationRequest.getEmail(), subject, mailContent, false, true, null, null);
        } catch (TemplateException | MessagingException | IOException ex) {
            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    /**
//     * Cache the join request confirmation in the EMail Spool cache.
//     *
//     * @param person the person the mail must be sent to
//     * @param organisation the organisation the person requested to join
//     */
//    @RolesAllowed("EquissentialUser")
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void sendJoinRequestConfirmation(Person person, Organisation organisation) {
//        try {
//            Map root = new HashMap();
//            root.put("name", person.getFirstName());
//            root.put("organisation", organisation.getName());
//            Template template = cfg.getTemplate("joinRequestConfirmation.ftl");
//            StringWriter writer = new StringWriter();
//            template.process(root, writer);
//            String mailContent = writer.toString();
//
//            String subject = person.getFirstName() + ", your request to join " + organisation.getName() + " has been submitted.";
//            writeToCache(person.getEmail(), subject, mailContent, false, true, null, null);
//        } catch (TemplateException | MessagingException | IOException ex) {
//            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Cache the join request in the EMail Spool cache.
//     *
//     * @param organisationAdmins a list of organisation admins
//     * @param organisation the organisation the person requested to join
//     * @param person the person making the join request
//     */
//    @RolesAllowed("EquissentialUser")
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void sendJoinRequest(List<Person> organisationAdmins, Organisation organisation, Person person) {
//        for (Person adminPerson : organisationAdmins) {
//            try {
//                Map root = new HashMap();
//                root.put("name", adminPerson.getFirstName());
//                root.put("fullName", person.getFirstName() + " " + person.getSurname());
//                root.put("organisation", organisation.getName());
//                Template template = cfg.getTemplate("joinRequest.ftl");
//                StringWriter writer = new StringWriter();
//                template.process(root, writer);
//                String mailContent = writer.toString();
//
//                String subject = organisation.getName() + " - member join request.";
//                writeToCache(adminPerson.getEmail(), subject, mailContent, false, true, null, null);
//            } catch (TemplateException | MessagingException | IOException ex) {
//                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//    
//    /**
//     * Cache the join request in the EMail Spool cache.
//     *
//     * @param organisation the organisation the person requested to join
//     * @param person the person making the join request
//     */
//    @RolesAllowed("EquissentialUser")
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void sendApproveJoinRequest(Organisation organisation, Person person) {
//        try {
//            Map root = new HashMap();
//            root.put("name", person.getFirstName());
//            root.put("organisation", organisation.getName());
//            Template template = cfg.getTemplate("joinRequestApproval.ftl");
//            StringWriter writer = new StringWriter();
//            template.process(root, writer);
//            String mailContent = writer.toString();
//
//            String subject = organisation.getName() + " - join request approved.";
//            writeToCache(person.getEmail(), subject, mailContent, false, true, null, null);
//        } catch (TemplateException | MessagingException | IOException ex) {
//            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Cache the join request in the EMail Spool cache.
//     *
//     * @param organisation the organisation the person requested to join
//     * @param person the person making the join request
//     * @param declineReason the reason the join request was declined
//     */
//    @RolesAllowed("EquissentialUser")
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
//    public void sendDeclineJoinRequest(Organisation organisation, Person person, String declineReason) {
//        try {
//            Map root = new HashMap();
//            root.put("name", person.getFirstName());
//            root.put("organisation", organisation.getName());
//            root.put("declineReason", declineReason);
//            Template template = cfg.getTemplate("joinRequestDecline.ftl");
//            StringWriter writer = new StringWriter();
//            template.process(root, writer);
//            String mailContent = writer.toString();
//
//            String subject = organisation.getName() + " - join request declined.";
//            writeToCache(person.getEmail(), subject, mailContent, false, true, null, null);
//        } catch (TemplateException | MessagingException | IOException ex) {
//            Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }    

    /**
     * Write the email to the DB cache.
     *
     * @param toAddresses the recipient address
     * @param subject the subject of the mail
     * @param body the content of the message
     * @param bccSupport true if support should be bcc'd, otherwise false
     * @param htmlContent true if the content is html, otherwise false
     * @param attachment the attachment to send
     * @param attachmentName the name of the attachment
     */
    private void writeToCache(String toAddresses, String subject, String body, boolean bccSupport, boolean htmlContent, byte[] attachment, String attachmentName) throws AddressException {
        InternetAddress.parse(toAddresses, true);

        Mail mail = new Mail();
        mail.setToAddress(toAddresses);

        String supportEmail = System.getProperty("EQUISSENTIAL_SUPPORT_EMAIL", "");

        mail.setFromAddress(supportEmail);
        mail.setStatus(NotificationStatus.PENDING.value());
        mail.setBody(body);
        mail.setReplyTo(supportEmail);
        mail.setSubject(subject);
        mail.setDateSubmitted(LocalDateTime.now());
        if (bccSupport) {
            mail.setBccAddress(supportEmail);
        }
        if (htmlContent == true) {
            mail.setContentType("HTML");
        } else {
            mail.setContentType("Text");
        }
        if (attachment != null) {
            mail.setAttachment(attachment);
        }
        mail.setAttachmentName(attachmentName);
        entityManager.persist(mail);
    }

    /**
     * Process the email spool every every minute.
     */
    @Schedule(hour = "*", minute = "*/1", dayOfWeek = "*", persistent = false)
    public void processMailCache() {
        Query query = entityManager.createNamedQuery("Email.findUnsent");
        query.setMaxResults(4);
        List<Mail> emailList = query.getResultList();
        for (Mail email : emailList) {
            try {
                sendCachedMail(email);
                email.setStatus(NotificationStatus.SENT.value());
                entityManager.merge(email);
            } catch (MessagingException ex) {
                Logger.getLogger(NotificationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Send the cached mail.
     *
     * @param mail the cached mail
     * @throws MessagingException if the message could not be sent
     */
    private void sendCachedMail(Mail mail) throws MessagingException {
        Logger.getLogger(NotificationController.class.getName()).log(Level.INFO, "Sending email");

        MimeMessage message = new MimeMessage(mailSession);
        message.setFrom(InternetAddress.parse(mail.getFromAddress())[0]);
        if (mail.getReplyTo() != null) {
            message.setReplyTo(InternetAddress.parse(mail.getReplyTo()));
        }
        if (mail.getCcAddress() != null) {
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(mail.getCcAddress()));
        }
        if (mail.getBccAddress() != null) {
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(mail.getBccAddress()));
        }
        message.setSubject(mail.getSubject());
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getToAddress(), false));

        if (mail.getContentType().equals("HTML") && mail.getAttachment() == null) {
            message.setDataHandler(new DataHandler(new HTMLDataSource(mail.getBody())));
        } else if (mail.getContentType().equals("HTML") && mail.getAttachment() != null) {
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setDataHandler(new DataHandler(new HTMLDataSource(mail.getBody())));
            MimeBodyPart mbp2 = buildAttachmentMimeBodyPart(mail.getAttachment(), mail.getAttachmentName(), "text/html");
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);
            message.setContent(mp);
        } else if (!mail.getContentType().equals("HTML") && mail.getAttachment() != null) {
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(mail.getBody());
            MimeBodyPart mbp2 = buildAttachmentMimeBodyPart(mail.getAttachment(), mail.getAttachmentName(), "text/plain");
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            mp.addBodyPart(mbp2);
            message.setContent(mp);
        } else {
            message.setText(mail.getBody());
        }
        String serverEnv = System.getProperty("EQUISSENTIAL_PROJECT_STAGE", "");
        if (serverEnv.equals("Production")) {
            Transport.send(message);
        } else {
            Logger.getLogger(NotificationController.class.getName()).log(Level.WARNING, "Not sending mail as it's on the test server.");
            Logger.getLogger(NotificationController.class.getName()).log(Level.INFO, mail.getBody());
        }
    }

    /**
     * Build a MimeBodyPart for the attachment.
     *
     * @param attachment the attachment
     * @param attachmentName the name of the attachment
     * @param type the MIME type
     * @return a mime body part
     * @throws MessagingException
     */
    private MimeBodyPart buildAttachmentMimeBodyPart(byte[] attachment, String attachmentName, String type) throws MessagingException {
        MimeBodyPart mbp2 = new MimeBodyPart();
        mbp2.setDataHandler(new DataHandler(new ByteArrayDataSource(attachment, type)));
        if (attachmentName != null) {
            mbp2.setFileName(attachmentName);
        } else {
            mbp2.setFileName("attachment");
        }
        return mbp2;
    }

    /*
     * Inner class to act as a JAF datasource to send HTML e-mail content
     */
    static class HTMLDataSource implements DataSource {

        private final String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        // Return html string in an InputStream.
        // A new stream must be returned each time.
        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) {
                throw new IOException("Null HTML");
            }
            return new ByteArrayInputStream(html.getBytes("UTF-8"));
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "JAF text/html dataSource to send e-mail only";
        }
    }
}
