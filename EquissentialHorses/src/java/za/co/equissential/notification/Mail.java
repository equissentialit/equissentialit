package za.co.equissential.notification;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

/**
 * Represents an email that is persisted.
 */
@Entity
@Table(name = "email_spool", schema = "equissential")
@NamedQueries({
    @NamedQuery(name = "Email.findUnsent", query = "SELECT e from Mail e WHERE e.status = 'Pending' ORDER BY e.dateSubmitted asc")})
public class Mail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "status")
    private String status;

    @Basic(optional = false)
    @Lob
    @Column(name = "body", length = 256000)
    private String body;

    @Basic(optional = false)
    @Column(name = "to_address", length = 256)
    private String toAddress;

    @Basic(optional = false)
    @Column(name = "from_address", length = 256)
    private String fromAddress;

    @Basic(optional = true)
    @Column(name = "bcc_address", length = 256)
    private String bccAddress;

    @Basic(optional = true)
    @Column(name = "cc_address", length = 256)
    private String ccAddress;

    @Basic(optional = false)
    @Column(name = "subject")
    private String subject;

    @Basic(optional = true)
    @Column(name = "reply_to")
    private String replyTo;

    @Basic(optional = false)
    @Column(name = "date_submitted")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateSubmitted;

    @Basic(optional = false)
    @Column(name = "content_type", length = 4)
    private String contentType;

    @Basic(optional = true)
    @Column(name = "attachment")
    private byte[] attachment;

    @Basic(optional = true)
    @Column(name = "attachment_name", length = 100)
    private String attachmentName;

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBccAddress() {
        return bccAddress;
    }

    public void setBccAddress(String bccAddress) {
        this.bccAddress = bccAddress;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCcAddress() {
        return ccAddress;
    }

    public void setCcAddress(String ccAddress) {
        this.ccAddress = ccAddress;
    }

    public LocalDateTime getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(LocalDateTime dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Mail)) {
            return false;
        }
        Mail other = (Mail) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "za.co.equissential.notification.Mail[ id=" + id + " ]";
    }

}
