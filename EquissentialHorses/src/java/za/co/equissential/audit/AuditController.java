package za.co.equissential.audit;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.annotation.security.PermitAll;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * Session Bean implementation class AuditController
 */
@Stateless
@LocalBean
@PermitAll
public class AuditController {

    @PersistenceContext
    private EntityManager em;

    public AuditController() {
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void audit(Principal principal, InvocationContext invocationContext, long executionTime) {
        Audit audit = new Audit();
        audit.setPerson(principal.getName());
        audit.setService(invocationContext.getMethod().getDeclaringClass().getSimpleName());
        audit.setTask(invocationContext.getMethod().getName());
        audit.setExecutionTime(executionTime);
        String timeStamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss"));
        audit.setTaskTime(timeStamp);
        em.persist(audit);
    }
}
