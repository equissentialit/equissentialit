package za.co.equissential.audit;

import java.security.Principal;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;


public class AuditInterceptor {

    @EJB
    private AuditController auditController;

    @Resource
    private EJBContext context;

    @AroundInvoke
    public Object intercept(InvocationContext invocationContext) throws Exception {
        Principal principal = context.getCallerPrincipal();
        long start = System.currentTimeMillis();
        Object result = invocationContext.proceed();        
        long executionTime = System.currentTimeMillis() - start;
        auditController.audit(principal, invocationContext, executionTime);
        return result;
    }
}
