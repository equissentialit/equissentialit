package za.co.equissential.horse.web;

import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import za.co.equissential.country.Country;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.horse.controller.HorseController;
import za.co.equissential.horse.entity.Horse;
import za.co.equissential.horse.entity.HorseBreed;
import za.co.equissential.horse.entity.HorseColour;
import za.co.equissential.person.entity.Person;
import za.co.equissential.util.BaseBean;
import za.co.equissential.util.EditMode;
import za.co.equissential.util.SessionUtil;
import za.co.equissential.util.SystemBean;

/**
 * The CategoryBean deals with the EMMA Category screens.
 */
@Named
@ViewScoped
public class HorseBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private SystemBean systemBean;

    @EJB
    private HorseController horseController;

    private List<Horse> horses;
    private final Person person;
    private List<Horse> selectedHorses;
    
    private String name;
    private String passportNumber;
    private LocalDateTime dateOfBirth;
    private String sire;
    private String dam;
    private String sireOfDam;
    private HorseColour colour;
    private HorseBreed breed;
    private Country countryOfOrigin;
    private String gender;
    private String classification;
    private String height;
    
    private EditMode editMode;
    private String dialogHeader;
    
    

    public HorseBean() {
        person = SessionUtil.getLoggedInUser();
    }

    @PostConstruct
    public void init() {
        try {
            horses = horseController.getHorses(person.getId());
        } catch (EntityNotFoundException ex) {
            Logger.getLogger(HorseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Person getPerson() {
        return person;
    }
    
    public List<Horse> getHorses() {
        return horses;
    }

    public List<String> getBooleanValues() {
        return Arrays.asList(new String[]{"true", "false"});
    }

    public SystemBean getSystemBean() {
        return systemBean;
    }

    public void setSystemBean(SystemBean systemBean) {
        this.systemBean = systemBean;
    }

    public List<Horse> getSelectedHorses() {
        return selectedHorses;
    }

    public void setSelectedHorses(List<Horse> selectedHorses) {
        this.selectedHorses = selectedHorses;
    }

    public String getDialogHeader() {
        return dialogHeader;
    }

    public void setDialogHeader(String dialogHeader) {
        this.dialogHeader = dialogHeader;
    }

    public void deleteHorse() {
        try {
            Horse horse = selectedHorses.get(0);
            horseController.deleteHorse(person.getId(), horse.getId());
            horses.remove(horse);
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityDeleted"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("horseMsgs", msg);
        } catch (EntityNotFoundException enfex) {
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityNotUpdated"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("deleteHorseMsgs", msg);
        }     
    }

    public void showEditDialog() {
        editMode = EditMode.EDIT;
        this.dialogHeader = MessageFormat.format(getBundle().getString("updateEntity"), getBundle().getString("horse"));
        Horse selectedHorse = selectedHorses.get(0);
        this.name = selectedHorse.getName();
        this.passportNumber = selectedHorse.getPassportNumber();
        this.breed = selectedHorse.getBreed();
        this.classification = selectedHorse.getClassification();
        this.colour = selectedHorse.getColour();
        this.countryOfOrigin = selectedHorse.getCountryOfOrigin();
        this.dam = selectedHorse.getDam();
        this.dateOfBirth = selectedHorse.getDateOfBirth();
        this.gender = selectedHorse.getGender();
        this.height = selectedHorse.getHeight();
        this.sire = selectedHorse.getSire();
    }

    public void showAddDialog() {
        editMode = EditMode.ADD;
        this.dialogHeader = MessageFormat.format(getBundle().getString("addEntity"), getBundle().getString("category"));
        this.name = null;
        this.passportNumber = null;
        this.breed = null;
        this.classification = null;
        this.colour = null;
        this.countryOfOrigin = null;
        this.dam = null;
        this.dateOfBirth = null;
        this.gender = null;
        this.height = null;
        this.sire = null;
    }

    public void saveHorse() throws EntityNotFoundException {
        if (editMode == EditMode.ADD) {
            addHorse();
        } else if (editMode == EditMode.EDIT) {
            updateHorse();
        }
    }

    private void updateHorse() {
        try {
            Horse horse = selectedHorses.get(0);
            
            horse.setBreed(breed);
            horse.setClassification(classification);
            horse.setColour(colour);
            horse.setCountryOfOrigin(countryOfOrigin);
            horse.setDam(dam);
            horse.setDateOfBirth(dateOfBirth);
            horse.setGender(gender);
            horse.setHeight(height);
            horse.setName(name);
            horse.setPassportNumber(passportNumber);
            horse.setSire(sire);
            horse.setSireOfDam(sireOfDam);
            
            horseController.updateHorse(person.getId(), horse);
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityUpdated"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("horseMsgs", msg);
            editMode = null;
        } catch (EntityNotFoundException enfex) {
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityNotUpdated"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("addHorseMsgs", msg);
        } 
    }

    private void addHorse() {
        try {
            Horse horse = new Horse();
            horse.setBreed(breed);
            horse.setClassification(classification);
            horse.setColour(colour);
            horse.setCountryOfOrigin(countryOfOrigin);
            horse.setDam(dam);
            horse.setDateOfBirth(dateOfBirth);
            horse.setGender(gender);
            horse.setHeight(height);
            horse.setName(name);
            horse.setPassportNumber(passportNumber);
            horse.setSire(sire);
            horse.setSireOfDam(sireOfDam);
            
            horseController.addHorse(person.getId(), horse);
            horses.add(horse);
            RequestContext.getCurrentInstance().addCallbackParam("validationFailed", false);
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityAdded"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("addHorseMsgs", msg);
            editMode = null;
        } catch (EntityNotFoundException enfex) {
            RequestContext.getCurrentInstance().addCallbackParam("validationFailed", true);
            FacesMessage msg = new FacesMessage(MessageFormat.format(getBundle().getString("entityNotFound"), getBundle().getString("horse")));
            FacesContext.getCurrentInstance().addMessage("addHorseMsgs", msg);
        }
    }
}
