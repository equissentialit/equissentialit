/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.horse.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import za.co.equissential.country.Country;

/**
 *
 * @author liezelbedggood
 */
@Entity
@Table(name = "horse", schema = "equissential")
@NamedQueries({
    @NamedQuery(name = "Horse.findByName", query = "SELECT h FROM Horse h where h.name = :name")
}) 
public class Horse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, 
                    generator = "horseGen")
    @TableGenerator(name = "horseGen",
                    schema = "equissential",
                    table = "sequence",
                    pkColumnName = "SEQ_NAME",
                    valueColumnName = "SEQ_COUNT",
                    pkColumnValue = "HORSE_SEQ",
                    allocationSize=1,
                    initialValue=0)
    @Column(name = "id")
    private Long id;

    @NotNull(message = "Name cannot be null")
    @Column(name = "horse_name")
    private String name;
    
    //@NotNull(message = "Passport number cannot be null")
    @Column(name = "passport_number")
    private String passportNumber;
    
    @Basic(optional = false)
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dateOfBirth;
    
    //@NotNull(message = "Sire cannot be null")
    @Column(name = "sire")
    private String sire;
    
    //@NotNull(message = "Dam cannot be null")
    @Column(name = "dam")
    private String dam;
    
    @Column(name = "sire_of_dam")
    private String sireOfDam;
    
    
    @JoinColumn(name = "colour_id", referencedColumnName = "id")
    @ManyToOne
    private HorseColour colour;
    
    @JoinColumn(name = "breed_id", referencedColumnName = "id")
    @ManyToOne
    private HorseBreed breed;
    
    @JoinColumn(name = "country_of_origin_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Country countryOfOrigin;
    
    //@NotNull(message = "Gender cannot be null")
    @Column(name = "gender")
    private String gender;
    
    //@NotNull(message = "Classification cannot be null")
    @Column(name = "horse_classification")
    private String classification;
    
    //@NotNull(message = "Height cannot be null")
    @Column(name = "height")
    private String height;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horse)) {
            return false;
        }
        Horse other = (Horse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "za.co.equissential.entity.Horse[ id=" + id + " ]";
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the passportNumber
     */
    public String getPassportNumber() {
        return passportNumber;
    }

    /**
     * @param passportNumber the passportNumber to set
     */
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * @return the sire
     */
    public String getSire() {
        return sire;
    }

    /**
     * @param sire the sire to set
     */
    public void setSire(String sire) {
        this.sire = sire;
    }

    /**
     * @return the dam
     */
    public String getDam() {
        return dam;
    }

    /**
     * @param dam the dam to set
     */
    public void setDam(String dam) {
        this.dam = dam;
    }

    /**
     * @return the sireOfDam
     */
    public String getSireOfDam() {
        return sireOfDam;
    }

    /**
     * @param sireOfDam the sireOfDam to set
     */
    public void setSireOfDam(String sireOfDam) {
        this.sireOfDam = sireOfDam;
    }

    /**
     * @return the colour
     */
    public HorseColour getColour() {
        return colour;
    }

    /**
     * @param colour the colour to set
     */
    public void setColour(HorseColour colour) {
        this.colour = colour;
    }

    /**
     * @return the breed
     */
    public HorseBreed getBreed() {
        return breed;
    }

    /**
     * @param breed the breed to set
     */
    public void setBreed(HorseBreed breed) {
        this.breed = breed;
    }

    /**
     * @return the countryOfOrigin
     */
    public Country getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * @param countryOfOrigin the countryOfOrigin to set
     */
    public void setCountryOfOrigin(Country countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the dateOfBirth
     */
    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the classification
     */
    public String getClassification() {
        return classification;
    }

    /**
     * @param classification the classification to set
     */
    public void setClassification(String classification) {
        this.classification = classification;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }
    
}
