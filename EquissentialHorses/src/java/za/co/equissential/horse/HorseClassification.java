/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.horse;

/**
 * Defines the classification of the horse
 * 
 * The classifications are as follows:
 *  <ul>
 *      <li>Horse - a horse is bigger than 14.2hh</li>
 *      <li>Pony - a 'horse' 14.2hh and smaller</li>
 *  </ul> 
 */
public enum HorseClassification {
    HORSE("Horse"), 
    PONY("Pony");

    private final String horseClassification;

    HorseClassification(String horseClassification) {
        this.horseClassification = horseClassification;
    }

    public String value() {
        return horseClassification;
    }  
    
}
