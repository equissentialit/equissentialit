/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.horse.controller;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import za.co.equissential.audit.AuditInterceptor;
import za.co.equissential.exceptions.EntityNotFoundException;
import za.co.equissential.horse.entity.Horse;
import za.co.equissential.person.entity.Address;
import za.co.equissential.person.entity.Person;

/**
 * Session Bean implementation class HorseController
 */
@Stateless
@LocalBean
@Interceptors(AuditInterceptor.class)
@RolesAllowed({"EquissentialUser"})
public class HorseController {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Default constructor.
     */
    public HorseController() {
    }

    public void setEntityManager(EntityManager em) {
        this.entityManager = em;
    }
    
    /**
     * Create a new address.
     *
     * @param personId the id of the person to add the address to
     * @param horse the horse to add
     * @throws EntityNotFoundException when an address is not found for the provided personId
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public long addHorse(long personId, @Valid Horse horse) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        entityManager.persist(horse);
        person.getHorses().add(horse);
        entityManager.merge(person);
        return horse.getId();
    }

        /**
     * Update an address.
     *
     * @param personId the person that owns the address
     * @param horse the horse to update
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateHorse(long personId, @Valid Horse horse) throws EntityNotFoundException {
        Horse storedhorse = entityManager.find(Horse.class, horse.getId());
        if (storedhorse == null) {
            throw new EntityNotFoundException(
                    "Entity " + Address.class.getSimpleName() + " with id " + horse.getId() + " not found.");
        }
        storedhorse.setBreed(horse.getBreed());
        storedhorse.setColour(horse.getColour());
        storedhorse.setCountryOfOrigin(horse.getCountryOfOrigin());
        storedhorse.setDam(horse.getDam());
        storedhorse.setGender(horse.getGender());
        storedhorse.setName(horse.getName());
        storedhorse.setPassportNumber(horse.getPassportNumber());
        storedhorse.setSire(horse.getSire());
        storedhorse.setSireOfDam(horse.getSireOfDam());
        
        entityManager.merge(storedhorse);
    }

    /**
     * Delete the specified address.
     *
     * @param personId the person that owns the horse
     * @param horseId the horse to delete
     * @throws EntityNotFoundException when a horse is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteHorse(long personId, long horseId) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        
        Horse horse = entityManager.find(Horse.class, horseId);
        if (horse == null) {
            throw new EntityNotFoundException(
                    "Entity " + Horse.class.getSimpleName() + " with id " + horseId + " not found.");
        }
        
        person.getHorses().remove(horse);
        entityManager.remove(horse);
    }

    /**
     * Get the specified horse.
     *
     * @param personId the person that owns the horse
     * @param horseId the horse to get
     * @return the specified horse
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Horse getHorse(long personId, long horseId) throws EntityNotFoundException {
        Horse horse = entityManager.find(Horse.class, horseId);
        if (horse == null) {
            throw new EntityNotFoundException(
                    "Entity " + Horse.class.getSimpleName() + " with id " + horseId + " not found.");
        }
        return horse;
    }
    
        /**
     * Get the specified horse.
     *
     * @param horseName the name of the horse
     * @return the specified horse
     * @throws EntityNotFoundException when an address is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Horse findHorseByName(String horseName) {
        Query query = entityManager.createNamedQuery("Horse.findByName");
        query.setParameter("name", horseName);
        try {
            return (Horse) query.getSingleResult();
        }
        catch (NoResultException noResultException) {
            return null;
        }
    }

    /**
     * Get all horses for the specified person.
     *
     * @param personId the person to get the horses for
     * @return a list of Horses for the specified person
     * @throws EntityNotFoundException when a person is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Horse> getHorses(long personId) throws EntityNotFoundException {
        Person person = entityManager.find(Person.class, personId);
        if (person == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + personId + " not found.");
        }
        return person.getHorses();
    }

        /**
     * Change the ownership of a horse to a different person
     *
     * @param currentOwnerId the id of the person who the horse used to belonged to
     * @param newOwnerId the id of the person who the horse now belongs to
     * @param horseId the id of the horse that is changing ownership
     * @throws EntityNotFoundException when a person is not found for the provided id
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void changeOwnership(long currentOwnerId, long newOwnerId, long horseId) throws EntityNotFoundException {
        Person currentOwner = entityManager.find(Person.class, currentOwnerId);
        if (currentOwner == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + currentOwnerId + " not found.");
        }
        Person newOwner = entityManager.find(Person.class, newOwnerId);
        if (newOwner == null) {
            throw new EntityNotFoundException(
                    "Entity " + Person.class.getSimpleName() + " with id " + newOwnerId + " not found.");
        }
        Horse horse = getHorse(currentOwnerId, horseId);
        currentOwner.getHorses().remove(horse);
        newOwner.getHorses().add(horse);
    }
    
    
}
