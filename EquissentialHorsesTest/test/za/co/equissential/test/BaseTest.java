package za.co.equissential.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONStringer;
import static org.junit.Assert.fail;

public abstract class BaseTest {

    protected static String cookie = null;
    public final static String BASE_URI = "http://localhost:8080/EquissentialHorses/rest/";
    public final static String AUTH_PERSON_BY_EMAIL = "auth/person/{email}";
    public final static String COUNTRIES = "countries";
    public final static String JOIN_REQUEST = "organisations/{organisationId}/joinRequests/{personId}";
    public final static String JOIN_REQUESTS_PENDING = "organisations/{organisationId}/joinRequests";
    public final static String LOGIN = "auth/login";
    public final static String LOGOUT = "auth/logout";
    public final static String ORGANISATIONS = "organisations";
    public final static String ORGANISATIONS_SHOWS = "organisations/{organisationId}/shows";
    public final static String ORGANISATIONS_SHOW_ENTRIES = "organisations/{organisationId}/shows/{showId}/entries";
    public final static String ORGANISATIONS_SHOW_SCHEDULE = "organisations/{organisationId}/shows/{showId}/schedule";
    public final static String ORGANISATIONS_BY_ID = "organisations/{organisationId}";
    public final static String ORGANISATIONS_JOIN = "organisations/{organisationId}/joinRequests/{personId}";
    public final static String PERSONS = "persons";
    public final static String PERSONS_PASSWORD = "persons/password";
    public final static String PERSONS_ADDRESSES = "persons/{personId}/addresses";
    public final static String PERSONS_ADDRESS = "persons/{personId}/addresses/{addressId}";
    public final static String PERSONS_BY_ID = "persons/{personId}";
    public final static String PERSONS_BY_EMAIL = "persons/email/{email}";
    public final static String REGISTER = "register";
    private HttpURLConnection conn;

    public BaseTest() {
    }

    public String getJson(HttpURLConnection conn) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }
        return builder.toString();
    }

    protected String getLiezelBedggood() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value("liezelbedggood@gmail.com");
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }

    public static int login(String json) throws IOException {
        int responseCode;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(BASE_URI + "auth/login");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();

            responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                cookie = conn.getHeaderField("Set-Cookie");
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return responseCode;
    }

    protected void loginLiezelBedggood() {
        try {
            String json = getLiezelBedggood();
            int responseCode = login(json);
            if (responseCode != 200) {
                json = getBruceBanner("liezelbedggood@gmail.com");
                responseCode = login(json);
                if (responseCode != 200) {
                    fail("Login failed - Liezel Bedggood");
                }
            }
        } catch (IOException ex) {
            fail("Login failed - Liezel Bedggood");
        }
    }

    protected void loginJeanGrey() {
        try {
            String json = getJeanGrey();
            int responseCode = login(json);
            if (responseCode != 200) {
                fail("Login failed - Jean Grey");
            }
        } catch (IOException ex) {
            fail("Login failed - Jean Grey");
        }
    }

    protected void loginSystemAdministrator() {
        try {
            String json = getMollyHayes();
            int responseCode = login(json);
            if (responseCode != 200) {
                fail("Login failed - Molly Hayes");
            }
        } catch (IOException ex) {
            fail("Login failed - Molly Hayes");
        }
    }

    protected void loginHarryOsborn() {
        try {
            String json = getHarryOsborn();
            int responseCode = login(json);
            if (responseCode != 200) {
                fail("Login failed - Harry Osborn");
            }
        } catch (IOException ex) {
            fail("Login failed - Harry Osborn");
        }
    }

    protected String getBruceBanner(String email) {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value(email);
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }

    protected String getTonyStark() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value("tony.stark@localhost");
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }
    
    protected String getMollyHayes() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value("molly.hayes@localhost");
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }
    
    protected String getJeanGrey() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value("jean.grey@localhost");
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }    

    protected String getHarryOsborn() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.object();
        jsonStringer.key("email").value("harry.osborn@localhost");
        jsonStringer.key("password").value("password");
        jsonStringer.endObject();
        return jsonStringer.toString();
    }

    public static void logout() throws IOException {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(BASE_URI + "auth/logout");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.disconnect();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public HttpURLConnection getSecureConnection(String path, String httpMethod) throws IOException {
        URL url = new URL(BASE_URI + path);
        conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(httpMethod);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Cookie", cookie);
        return conn;
    }

    public void disconnect() {
        conn.disconnect();
    }
}
