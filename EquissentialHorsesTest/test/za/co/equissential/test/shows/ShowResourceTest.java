/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.equissential.test.shows;

import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONStringer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import za.co.equissential.test.BaseTest;

/**
 *
 * @author liezelbedggood
 */
public class ShowResourceTest extends BaseTest {

    public ShowResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // 
    @Test
    public void testGetShows() throws IOException {
        loginLiezelBedggood();

        // Get all Shows for an organisation
        JsonPath jsonPath
                = given().
                        cookie(cookie).
                        contentType("application/json").
                        pathParam("organisationId", 3).
                        when().
                        get(BASE_URI + ORGANISATIONS_SHOWS).
                        then().
                        assertThat().
                        statusCode(200).
                        extract().jsonPath();

        System.out.println(jsonPath.toString());

        assertTrue(jsonPath.getList("").size() == 1);

        logout();
    }

    @Test
    public void testLoadEntries() throws IOException {
        loginLiezelBedggood();
//        String entries = getJsonShowEntry();
        String entries = getShowEntriesFromFile(new File("/Users/liezelbedggood/Documents/EquissentialITServices/SANESA/SANESAQ4PrimaryEntries.txt"));
        System.out.println(entries);
        // load competitors for a specific show
        assertNotNull(entries);
        given().
                cookie(cookie).
                contentType("application/json").
                body(entries).
                pathParam("organisationId", new Long(3)).
                pathParam("showId", new Long(1)).
                when().
                post(BASE_URI + ORGANISATIONS_SHOW_ENTRIES).
                then().
                assertThat().
                statusCode(201);

        logout();
    }
    
    @Test
    public void testCreateShowSchedule() throws IOException {
        loginLiezelBedggood();

        // Get all Shows for an organisation
        JsonPath jsonPath
                = given().
                        cookie(cookie).
                        contentType("application/json").
                        pathParam("organisationId", 3).
                        pathParam("showId", new Long(1)).
                        when().
                        get(BASE_URI + ORGANISATIONS_SHOW_SCHEDULE).
                        then().
//                        assertThat().
//                        statusCode(200).
                        extract().jsonPath();

                        System.out.println(jsonPath.toString());

        logout();
    }

    private String getJsonShowEntry() {
        JSONStringer jsonStringer = new JSONStringer();
        jsonStringer.array();
            jsonStringer.object();
            jsonStringer.key("showClassCode").value("P1SJ03");
            jsonStringer.key("riderName").value("Aiden");
            jsonStringer.key("riderSurname").value("Ungerer");
            jsonStringer.key("horseName").value("China Black");
            jsonStringer.key("schoolName").value("Grantleigh School");
            jsonStringer.endObject();
            jsonStringer.object();
            jsonStringer.key("showClassCode").value("P1EQ03");
            jsonStringer.key("riderName").value("Aiden");
            jsonStringer.key("riderSurname").value("Ungerer");
            jsonStringer.key("horseName").value("China Black");
            jsonStringer.key("schoolName").value("Grantleigh School");
            jsonStringer.endObject();
            jsonStringer.object();
            jsonStringer.key("showClassCode").value("P1WR03");
            jsonStringer.key("riderName").value("Aiden");
            jsonStringer.key("riderSurname").value("Ungerer");
            jsonStringer.key("horseName").value("China Black");
            jsonStringer.key("schoolName").value("Grantleigh School");
            jsonStringer.endObject();
            jsonStringer.object();
            jsonStringer.key("showClassCode").value("P3DR07b");
            jsonStringer.key("riderName").value("Amy");
            jsonStringer.key("riderSurname").value("Diedericks");
            jsonStringer.key("horseName").value("Foresyte Soft Landing");
            jsonStringer.key("schoolName").value("Pelham Senior Primary School");
            jsonStringer.endObject();
        jsonStringer.endArray();
        return jsonStringer.toString();
    }

    private String getShowEntriesFromFile(File file) {
        try {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new FileReader(file));
            JSONStringer jsonStringer = new JSONStringer();
            jsonStringer.array();
            try {
                String line;
                while ((line = input.readLine()) != null) {
                    if (line.startsWith("Class")) {
                        String classCode = line.substring((line.indexOf(" ") + 1), line.length());
                        if (classCode.isEmpty()) {
                            continue;
                        }
                        jsonStringer.object();
                        jsonStringer.key("showClassCode").value(classCode);
                        String riderFullName = input.readLine();
                        String riderName = riderFullName.substring(0, riderFullName.indexOf(" "));
                        String riderSurname = riderFullName.substring(riderFullName.indexOf(" ")+1,riderFullName.length());
                        jsonStringer.key("riderName").value(riderName);
                        jsonStringer.key("riderSurname").value(riderSurname);
                        String horseName = input.readLine();
                        jsonStringer.key("horseName").value(horseName);
                        String schoolName = input.readLine();
                        jsonStringer.key("schoolName").value(schoolName);
                        jsonStringer.endObject();
                    }

                }
            } finally {
                input.close();
            }
            
            jsonStringer.endArray();
            return jsonStringer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
}
